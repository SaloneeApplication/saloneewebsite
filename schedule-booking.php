<?php 
include('header.php');
$page = 'schedule';

$user_id = @$_SESSION['user_id'];

if($user_id == "")
{
    echo '<script> var base_url = "http://localhost/salonee_web/"; </script>';
    echo '<script> window.location.replace(base_url); </script>';
}

include("config/dbConnection.php");
include("config/constants.php");
include("controllers/functions.php");

$dbObject = new dbConnection();
$con = $dbObject->getConnection();
$funcObject = new functions();

$bookings = $funcObject->bookings($con, $user_id);

?>
<link id="bsdp-css" href="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet">
<div class="container-fluid">
    <div class="_header"></div>
    <!-- breadcrumb  -->
    <nav aria-label="breadcrumb" class="_custmBrdcrmb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">My Account</a></li>
            <li class="breadcrumb-item active" aria-current="page">Schedule Booking</li>
        </ol>
    </nav>
    <div class="d-flex myFlex">
      <?php include('sidebar.php');?>
        <div class="mainDiv _bgWyt">
            <?php
            foreach ($bookings as $row) 
            { ?>
                <div class="card _bookngsCrd">
                    <div class="crdImg">
                        <img src="<?php echo $row['image'];?>" alt="">
                    </div>
                    <div class="crd_desc">
                        <h5 class="card-title"><?php echo $row['business_name'];?></h5>
                        <div class="threeFive">
                            <div class="stars-outer">
                                <div class="stars-inner" style="width: 70%;"></div>
                            </div>
                            <span class="number-rating">3.5</span>
                        </div>
                        <p><i><img src="assets/img/icons/phone-call.png" alt="phone-call"></i><a href="tel:+971 <?php echo $row['mobile'];?>">+971 <?php echo $row['mobile'];?></a></p>
                        <p><i><img src="assets/img/icons/loc.png" alt="loc"></i><span><?php echo $row['address'];?></span></p>
                        <p>Service: <?php echo $row['service_name'];?> </p>
                        <p><i><img src="assets/img/icons/clock.png" alt="clock"></i><span><?php echo date('d-M-Y H:i A', strtotime($row['slot_date']));?> </span></p>
                        <h6><b>Price: <?php echo $row['total_amount'];?> AED</b></h6>
                    </div>
                    <div class="crd_Btns">
                        <a href="javascript:;" onclick="reschdule(<?php echo $row['slot_id'];?>, <?php echo $row['service_provider_id'];?>)" class="theme-btn _greyBg">Reschedule Booking</a>
                        <a href="javascript:;" onclick="cancel(<?php echo $row['slot_id'];?>)" class="theme-btn _cancel">Cancel Booking</a>
                    </div>
                </div>
            <?php
            }?>
        </div>
    </div> 
</div>
<?php include('footer.php');?>

<div class="modal fade pswdModal" id="addTimings" tabindex="-1" role="dialog"
    aria-labelledby="addDiscountTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="col-12 modal-title text-center" id="addDiscountTitle">Slot Reschedule</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="login">
                    <form>
                        <input type="hidden" name="type" value="rescheduleSlotTime" />
                        <input type="hidden" name="sid" id="sid" value="" />
                        <input type="hidden" name="spid" id="spid" value="" />
                        <div class="form-group">
                            <input type="text"  id="slot_date" name = "slot_date" class="form-control" autocomplete="off" />
                            <label class="form-control-placeholder p-0"  for="slot_date">Slot Date</label>
                        </div>
                        <div class="form-group">
                            <div class="boxedList" id="slots_data" style="display:none;">
                                <div class="flxRow">
                                    <p>Select Available Slot</p>
                                </div>
                                <div id="slotsList" style="">
                                    <ul>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="button" id="reschedule-btn" class="btn theme-btn"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    function reschdule(sid, spid)
    {
        $("#spid").val(spid);
        $("#sid").val(sid);
        $("#addTimings").modal('show');
    }

    function cancel(sid)
    {
        if (!confirm('Are you sure?')) return false;
        
        $("#sid").val(sid);

        var slot_id = $("#sid").val();

        var formData = new FormData();

        formData.append('slot_id', slot_id);

        $.ajax({
            url:"controllers/cancel_slot.php",
            method:"POST",
            data:formData,
            contentType: false,
            processData: false,
            success:function(res){
                var res = JSON.parse(res);
                var status = res.status;
                var message = res.message;

                if(status == 200){
                    swal({
                        type: "success",
                        text: message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                    setInterval('location.reload()', 1500); 
                }   
                else
                {
                    swal({
                        type: "error",
                        text: message,
                        showConfirmButton: false,
                        timer: 2500
                    });
                } 
            }
        });
    }

    $('#slot_date').datepicker({
        format: 'yyyy-mm-dd',
        todayHighlight:true,
        autoclose:true,
        startDate:"<?php echo date("Y-m-d");?>",
        endDate :"<?php echo date("Y-m-d",strtotime("+20 days"));?>"

    });

    $("#slot_date").change(function(){
        $("#slots_data").show();
        $("#slotsList ul").html('<center>Loading...</center>');
        var slot_id = $("#sid").val();
        var service_provider_id = $("#spid").val();
        var slot_date = $(this).val();
        $.ajax({
            url:"controllers/get_slots.php",
            method:"POST",
            data:{slot_id:slot_id,service_provider_id:service_provider_id,slot_date:slot_date},
            success:function(data){
                $("#slots_data").show();
                $("#slotsList ul").html(data);
            }
        });
    });

    $("#reschedule-btn").click(function(){
        
        var user_id = '<?php echo $user_id;?>';
        var slot_id = $("#sid").val();
        var service_provider_id = $("#spid").val();
        var slot_date = $('#slot_date').val();
        var slot_time = $('input:radio[name=slot_time]:checked').val();

        var formData = new FormData();

        formData.append('slot_id', slot_id);
        formData.append('service_provider_id', service_provider_id);
        formData.append('slot_date', slot_date);
        formData.append('slot_time', slot_time);
        formData.append('user_id', user_id);

        $.ajax({
            url:"controllers/reschedule_slot.php",
            method:"POST",
            data:formData,
            contentType: false,
            processData: false,
            success:function(res){
                var res = JSON.parse(res);
                var status = res.status;
                var message = res.message;

                if(status == 200){
                    swal({
                        type: "success",
                        text: message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                    setInterval('location.reload()', 1500); 
                }   
                else
                {
                    swal({
                        type: "error",
                        text: message,
                        showConfirmButton: false,
                        timer: 2500
                    });
                } 
            }
        });
    });
</script>