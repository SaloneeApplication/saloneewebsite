// image upload 
$('#file-upload').change(function() {
  var i = $(this).prev('label').clone();
  var file = $('#file-upload')[0].files[0].name;
  $(this).prev('label').text(file);
});


// FLOATING LABLE OF INPUT 
$(document).ready(function() {
  $("input").focus(function(){        
      // $(this).siblings("input").find(".active").removeClass("active");
      $(this).parent().addClass("active");   
});
  if($(this).value =='null'){
      $("input").focusout(function(){    
              $(this).parent().removeClass("active");    
          
      });
  }
});


// owlCarousel 
$(document).ready(function(){

  // category slider 
  $(".category-slider").owlCarousel({
      items:6,
      itemsDesktop:[1000,6],
      itemsDesktopSmall:[979,6],
      itemsTablet:[768,3],
      autoplayHoverPause : true,
      pagination:false,
      navigation:true,
      navigationText:["",""],
      autoPlay:true
  });

// news slider 
  $(".news-slider").owlCarousel({
    items:2,
    itemsDesktop:[1000,2],
    itemsDesktopSmall:[979,2],
    itemsTablet:[768,2],
    autoplayHoverPause : true,
    pagination:false,
    navigation:true,
    navigationText:["",""],
    autoPlay:true
});
 
// owl slider 
$(".services2-slide").owlCarousel({
  smartSpeed: 3000,
  items:4,
  itemsDesktop:[1000,4],
  itemsDesktopSmall:[979,4],
  itemsTablet:[768,2],
  autoplayHoverPause : true,
  pagination:false,
  navigation:true,
  navigationText:["<i class='fa fa-arrow-left'></i>","<i class='fa fa-arrow-right'></i>"],
  autoPlay:true
});



// owl slider 
$(".services-slide").owlCarousel({
  smartSpeed: 3000,
  items:4,
  itemsDesktop:[1000,4],
  itemsDesktopSmall:[979,4],
  itemsTablet:[768,2],
  autoplayHoverPause : true,
  pagination:false, 
  navigation: true,
  navigationText:["<i class='fa fa-arrow-left'></i>","<i class='fa fa-arrow-right'></i>"],
  autoPlay:true
});

});

 



// **************country flag with mobile input ***************

var telInput = $("#phone"),
  errorMsg = $("#error-msg"),
  validMsg = $("#valid-msg");

// initialise plugin
telInput.intlTelInput({

  allowExtensions: true,
  formatOnDisplay: true,
  autoFormat: true,
  autoHideDialCode: true,
  autoPlaceholder: true,
  defaultCountry: "auto",
  ipinfoToken: "yolo",

  nationalMode: false,
  numberType: "MOBILE",
  //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
  preferredCountries: ['sa', 'ae', 'qa','om','bh','kw','ma'],
  preventInvalidNumbers: true,
  separateDialCode: true,
  initialCountry: "ae",
  geoIpLookup: function(callback) {
  $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
    var countryCode = (resp && resp.country) ? resp.country : "";
    callback(countryCode);
  });
},
   utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"
});

var reset = function() {
  telInput.removeClass("error");
  errorMsg.addClass("hide");
  validMsg.addClass("hide");
};

// on blur: validate
telInput.blur(function() {
  reset();
  if ($.trim(telInput.val())) {
    if (telInput.intlTelInput("isValidNumber")) {
      validMsg.removeClass("hide");
    } else {
      telInput.addClass("error");
      errorMsg.removeClass("hide");
    }
  }
});

// on keyup / change flag: reset
telInput.on("keyup change", reset);





// ****************otp input form ***************

$(function() {
  'use strict';

  var body = $('body');

  function goToNextInput(e) {
    var key = e.which,
      t = $(e.target),
      sib = t.next('.otpInput');

    if (key != 9 && (key < 48 || key > 57)) {
      e.preventDefault();
      return false;
    }

    if (key === 9) {
      return true;
    }

    if (!sib || !sib.length) {
      sib = body.find('.otpInput').eq(0);
    }
    sib.select().focus();
  }

  function onKeyDown(e) {
    var key = e.which;

    if (key === 9 || (key >= 48 && key <= 57)) {
      return true;
    }

    e.preventDefault();
    return false;
  }
  
  function onFocus(e) {
    $(e.target).select();
  }

  body.on('keyup', '.otpInput', goToNextInput);
  body.on('keydown', '.otpInput', onKeyDown);
  body.on('click', '.otpInput', onFocus);

})

// ----------------  Toggle the menu on click of nav-link ----------
$('.menu li a').on('click', function(){
  $('.menu').toggleClass('active')
  $('#menu-toggle-form').prop('checked', false);
  
  });

