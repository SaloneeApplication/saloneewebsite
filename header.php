<?php session_start();

include("config/dbConnection.php");
include("config/constants.php");
include("controllers/functions.php");

$dbObject = new dbConnection();
$con = $dbObject->getConnection();
$funcObject = new functions();

$nav_links = $funcObject->nav_links($con);

while($row=mysqli_fetch_array($nav_links)) {
   $nav_link_data[] = $row;
}

?>

<!DOCTYPE html>
<html lang="en" dir="<?php echo @$_SESSION['lang'] == 'Arabic' ? 'rtl' : 'ltr';?>">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Salonee" />
		<meta name="author" content="#" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<title>Salonee | it's your time to shine</title>
		<link rel="shortcut icon" href="assets/img/favicon.png" />
	
		<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.0.8/css/all.css'>
		<link rel="stylesheet" href="assets/css/style.css" />
		<link rel="stylesheet" href="assets/css/inner-page.css" />
		<link rel="stylesheet" href="assets/css/owl.theme.min.css"/>
		<link rel="stylesheet" href="assets/css/owl_css.css"/>
		<link rel="stylesheet" href="assets/css/countryCode.css" />

		<?php 
		if(@$_SESSION['lang'] == 'Arabic')
		{?>
			<link rel="stylesheet" href="assets/css-rtl/bootstrap.min.css"/>
			<link rel="stylesheet" href="assets/font-awesome-4.7.0/css/font-awesome.min.css" />
			<link rel="stylesheet" href="assets/css-rtl/animate.css" />
			<link rel="stylesheet" href="assets/css-rtl/owl.carousel.css" />

			<link rel="stylesheet" type="text/css" href="assets/revolution-rtl/css/rs6.css">
			<link rel="stylesheet" type="text/css" href="assets/css-rtl/main.css"/>
			<link rel="stylesheet" href="assets/css-rtl/bootstrap-select.min.css">

			<link rel="stylesheet" type="text/css" href="assets/css-rtl/flaticon.css"/>
			<link rel="stylesheet" type="text/css" href="assets/css-rtl/prettyPhoto.css">
			<link rel="stylesheet" type="text/css" href="assets/css-rtl/shortcodes.css"/>
			<link rel="stylesheet" type="text/css" href="assets/css-rtl/responsive.css"/>
		<?php
		}else
		{?>
			<link rel="stylesheet" href="assets/bootstrap-4.3.1/css/bootstrap.min.css"/>
			<link rel="stylesheet" href="assets/font-awesome-4.7.0/css/font-awesome.min.css" />
			<link rel="stylesheet" href="assets/css/animate.css" />
			<link rel="stylesheet" href="assets/css/owl.carousel.css" />

			<link rel="stylesheet" type="text/css" href="assets/revolution/css/rs6.css">
			<link rel="stylesheet" type="text/css" href="assets/css/shortcodes.css"/>
			<link rel="stylesheet" type="text/css" href="assets/css/main.css"/>
			<link rel="stylesheet" href="assets/css/bootstrap-select.min.css">

			<link rel="stylesheet" type="text/css" href="assets/css/flaticon.css"/>
			<link rel="stylesheet" type="text/css" href="assets/css/prettyPhoto.css">
			<link rel="stylesheet" type="text/css" href="assets/css/responsive.css"/>
		<?php
		}?>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />
		
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css" integrity="sha512-O03ntXoVqaGUTAeAmvQ2YSzkCvclZEcPQu1eqloPaHfJ5RuNGiS4l+3duaidD801P50J28EHyonCV06CUlTSag==" crossorigin="anonymous" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css"/>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/css/intlTelInput.css" rel="stylesheet" media="screen">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.0.4/popper.js"></script>
		<style>
            .btn{
                background: #d55c91!important;
                color: white!important;
            }
            .header-btn a:first-child{
	          	border-left:none !important;
	      	}
        </style>
	</head>
	<body>
		<div class="page">
			<div id="preloader">
				<div id="status">&nbsp;</div>
			</div>
			<header id="masthead" class="header ttm-header-style-01">
				<div class="ttm-header-wrap">
					<div id="ttm-stickable-header-w" class="ttm-stickable-header-w clearfix">
						<div class="ttm-topbar-wrapper ttm-textcolor-white clearfix">
							<div class="container">
								<div class="row">
									<div class="col-12">
										<div class="ttm-topbar-content">
											<ul class="top-contact text-left">
												<li style="display:none">
													<a id="language-dropdown" class="font-weight-600" href="#">
														<select title="" class="selectpicker" data-live-search="true" style="" onchange="lang_session(this.value);">
															<!-- <option <?php echo @$_SESSION['lang'] == 'Arabic' ? 'selected':'';?> value="Arabic"> Arabic</option> -->
															<option <?php echo @$_SESSION['lang'] == 'English' ? 'selected':'';?> value="English"> English</option>
														</select>
													</a>
												</li>
												<li><i class="fa fa-envelope-o ttm-textcolor-skincolor"></i><a href="mailto:info@mysalonee.com">info@mysalonee.com</a></li>
											</ul>
											<div class="topbar-right text-right">
												<div class="header-btn">	
													<a class="font-weight-600" target="_blank" href="partner.php">PARTNER WITH SALONEE</a>				
													<a class="font-weight-600" href="javascript:;" data-toggle="modal" data-target="#locationModal"><i class="fa fa-map-marker ttm-textcolor-skincolor"></i>
													<?php
													if(isset($_SESSION) && @$_SESSION['city_name'] != "")
													{
														echo @$_SESSION['city_name']; 
													}else{
													?>
														<?php echo $nav_link_data[0]['locator'];?>
													<?php
													}?>
													</a>
													<?php
													if(isset($_SESSION) && @$_SESSION['username'] != "")
													{?>
														<a class="font-weight-600" href="myAccount.php"><?php echo $nav_link_data[0]['my_account'];?></a>
														<a class="font-weight-600" href="logout.php"><?php echo $nav_link_data[0]['logout'];?></a>
													<?php
													}else{
													?>
														<a class="font-weight-600" href="#" data-toggle="modal" data-target="#loginModal"><?php echo $nav_link_data[0]['login'];?></a>
														<a class="font-weight-600" href="#" data-toggle="modal" data-target="#signupModal"><?php echo $nav_link_data[0]['register'];?></a>
													<?php
													}?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="site-header-menu" class="site-header-menu">
							<div class="site-header-menu-inner ttm-stickable-header">
								<div class="container">
									<div class="row">
										<div class="col">
											<div id="site-navigation" class="site-navigation d-flex flex-row">
												<div class="site-branding mr-auto">
													<a class="home-link" href="index.php" title="Sylin Beauty" rel="home">
													<img id="logo-img" class="img-center lazyload" src="assets/img/small-logo.png" alt="logo">
													</a>
												</div>
												<div class="ttm-menu-toggle">
													<input type="checkbox" id="menu-toggle-form" />
													<label for="menu-toggle-form" class="ttm-menu-toggle-block">
													<span class="toggle-block toggle-blocks-1"></span>
													<span class="toggle-block toggle-blocks-2"></span>
													<span class="toggle-block toggle-blocks-3"></span>
													</label>
												</div>
												<!-- <nav id="menu" class="menu">
													<ul class="dropdown">
														<li><a href="index.php">HOME</a></li>
														<li><a href="#aboutus-box">ABOUT US</a></li>
														<li><a href="#category-box">CATEGORIES</a></li>
														<li><a href="#pricecompare-box">COMPARE PRICES</a></li>
														<li><a href="saloon-shops.php">NEAR ME</a></li>
														<li><a href="#feature-box">FEATURES</a></li>
														<li><a href="#download-box">DOWNLOAD APP</a></li>
													</ul>
												</nav> -->
												<nav id="menu" class="menu">
                                                <ul class="dropdown">
                                                   	<li><a href="index.php#aboutus-box"><?php echo $nav_link_data[0]['about_us'];?></a></li>
                                                   	<li><a href="index.php#feature-box"><?php echo $nav_link_data[0]['features'];?></a></li>
													<li><a href="index.php#category-box"><?php echo $nav_link_data[0]['categories'];?></a></li>
                                                    <!-- <li><a href="index.php#pricecompare-box"><?php echo $nav_link_data[0]['compare_prices'];?></a></li> -->
													<li><a href="blog.php"><?php echo $nav_link_data[0]['blog'];?></a></li>
													<li><a class="d-block d-lg-none d-xl-none"  href="partner.php"><?php echo $nav_link_data[0]['partner_with_salonee'];?></a></li>
													<li>
														<a href="index.php#download-box">
															<img style="height: 35px;" src ="assets/img/DOWNLOAD.svg" />
														</a>
													</li>
													<li>
														<button onclick="location.href='https://mysalonee.com/serviceprovider/register.php';" class="btn-sm ttm-btn ttm-btn-style-fill ttm-btn-color-skincolor ml-auto" style="height: 35px;
														padding: 5px;"><?php echo $nav_link_data[0]['join_us'];?></button>
													</li>
                                                </ul>
                                            </nav>
											</div>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="d-block d-sm-block d-md-none d-lg-none">
										<span class="toggleTopbarItems"><i class="fa fa-chevron-circle-down" aria-hidden="true"></i></span>
										<div class="topBarItems" style="display: none;">
											<div class="col-12">
												<div class="ttm-topbar-content">
													<ul class="top-contact text-left">
														<!-- <li>
															<a id="language-dropdown" class="font-weight-600" href="#">
																<select title="" class="selectpicker" data-live-search="true" style="">
																	<option data-content="<img src='assets/img/flags/uae.png' class='img-fluid'><span> Arabic</span>"> Arabic</option>
																	<option data-content="<img src='assets/img/flags/uk.png' class='img-fluid'><span> English UK</span>"> English UK</option>
																	<option data-content="<img src='assets/img/flags/usa.png' class='img-fluid'><span> English USA</span>"> English USA</option>
																</select>
															</a>
														</li> -->
														
														<li><i class="fa fa-envelope-o ttm-textcolor-skincolor"></i><a href="mailto:info@mysalonee.com">info@mysalonee.com</a></li>
														<li><a class="patrner-text" target="_blank" href="partner.php">PARTNER WITH SALONEE</a>	</li>
													</ul>
													<div class="topbar-right text-right">
														<div class="header-btn">	
																											
															<a class="font-weight-600" href="#" data-toggle="modal" data-target="#locationModal"><i class="fa fa-map-marker ttm-textcolor-skincolor"></i>
															<?php
															if(isset($_SESSION) && @$_SESSION['city_name'] != "")
															{
																echo @$_SESSION['city_name']; 
															}else{
															?>
																LOCATOR
															<?php
															}?>
															</a>
															<?php
															if(isset($_SESSION) && @$_SESSION['username'] != "")
															{?>
																<a class="font-weight-600" href="myAccount.php">My Account</a>
																<a class="font-weight-600" href="logout.php">LOGOUT</a>
															<?php
															}else{
															?>
																<a class="font-weight-600" href="#" data-toggle="modal" data-target="#loginModal">LOGIN</a>
																<a class="font-weight-600" href="#" data-toggle="modal" data-target="#signupModal">REGISTER</a>
															<?php
															}?>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>			