<?php 
include('header.php');
$page = 'history';

$user_id = @$_SESSION['user_id'];

if($user_id == "")
{
    echo '<script> var base_url = "http://localhost/salonee_web/"; </script>';
    echo '<script> window.location.replace(base_url); </script>';
}

include("config/dbConnection.php");
include("config/constants.php");
include("controllers/functions.php");

$dbObject = new dbConnection();
$con = $dbObject->getConnection();
$funcObject = new functions();

$bookings = $funcObject->bookingHistory($con, $user_id);


?>
<div class="container-fluid">
    <div class="_header"></div>
    <!-- breadcrumb  -->
    <nav aria-label="breadcrumb" class="_custmBrdcrmb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">My Account</a></li>
            <li class="breadcrumb-item active" aria-current="page">Booking History</li>
        </ol>
    </nav>
    <div class="d-flex myFlex">
        <?php include('sidebar.php');?>
        <div class="mainDiv _bgWyt">
            <?php
            foreach ($bookings as $row) 
            {?>
                <div class="card _bookngsCrd">
                    <div class="crdImg">
                        <img src="<?php echo $row['image'];?>" alt="">
                    </div>
                    <div class="crd_desc">
                        <h5 class="card-title"><?php echo $row['business_name'];?></h5>
                        <p><i><img src="assets/img/icons/loc.png" alt="loc"></i><span><?php echo $row['address'];?></span></p>
                        <p>Service: <?php echo $row['service_name'];?> </p>
                        <p><i><img src="assets/img/icons/clock.png" alt="clock"></i><span><?php echo date('d-M-Y H:i A', strtotime($row['slot_date']));?> </span></p>
                        <h6><b>Price: <?php echo $row['total_amount'];?> AED</b></h6>
                    </div>
                    <div class="crd_Btns">
                        <a href="#" class="theme-btn _greyBg">Book Again</a>
                        <a href="#" data-toggle="modal" onclick="addRating(<?php echo $row['service_provider_id'];?>, <?php echo $row['service_provider_service_id'];?>)" data-target="#RatingModal" class="theme-btn _grn">Add Rating</a>
                    </div>
                </div>
            <?php
            }?>
        </div>
        <!--end Main Div-->
    </div>
</div>
<?php include('footer.php');?>

<div class="modal fade pswdModal" id="RatingModal" tabindex="-1" role="dialog" aria-labelledby="RatingModalTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="col-12 modal-title text-center" id="RatingModalTitle">Rating For Service</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="login">
                    <form>
                        <input type="hidden" name="spid" id="spid" value="" />
                        <input type="hidden" name="service_id" id="service_id" value="" />
                        <div class="form-group mb-3">
                            <h6 class="mb-2" id="business_name">  </h6>
                            <input class="rating" id="rating" name="rating" value="">
                        </div>
                        <div class="form-group mb-5">
                            <textarea type="text" id="description" rows="5" class="form-control" autocomplete="off"
                                required></textarea>
                            <label class="form-control-placeholder p-0" for="accountNumber">Description</label>
                        </div>
                        <div class="form-group">
                            <button type="button" id="addRating" class="btn theme-btn" id="success">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="assets/js/simple-rating.js"></script>
<script>
    $(document).ready(function(){
      $('.rating').rating();
    });

    function addRating(spid, service_id)
    {
        $("#spid").val(spid);
        $("#service_id").val(service_id);

        var service_provider_id = $("#spid").val();
        var service_id = $("#service_id").val();
        var user_id = "<?php echo $user_id;?>";

        $.ajax({
            url:"controllers/get_rating.php",
            method:"POST",
            data:{service_provider_id:service_provider_id,service_id:service_id,user_id:user_id},
            success:function(res){
                var res = JSON.parse(res);
                var res = res.service_provider;

                $("#business_name").text(res[0].business_name);

                $("#rating").val(res[1].rating);
                $("#description").val(res[1].comment);

                var rating = res[1].rating; 
                rating = parseInt(rating) + 1;

                for(var i = 0; i < rating; i++)
                {
                    $("[data-rating='"+ i +"']").removeClass('fa-star-o');
                    $("[data-rating='"+ i +"']").addClass('fa-star');
                }
            }
        });
    }

    $("#addRating").click(function(){

        var service_provider_id = $("#spid").val();
        var service_id = $("#service_id").val();
        var rating = $("#rating").val();
        var description = $("#description").val();
        var user_id = "<?php echo $user_id;?>";

        $.ajax({
            url:"controllers/insert_rating.php",
            method:"POST",
            data:{service_provider_id:service_provider_id, service_id:service_id, rating:rating, description:description, user_id:user_id},
            success:function(res){

                if(res == 1){
                    swal({
                        type: "success",
                        text: 'Rating added successfully.',
                        showConfirmButton: false,
                        timer: 1500
                    });
                    setInterval('location.reload()', 1500); 
                }   
                else if(res == 3){
                    swal({
                        type: "success",
                        text: 'Rating updated successfully.',
                        showConfirmButton: false,
                        timer: 1500
                    });
                    setInterval('location.reload()', 1500); 
                }   
                else
                {
                    swal({
                        type: "error",
                        text: 'Something went wrong.',
                        showConfirmButton: false,
                        timer: 2500
                    });
                } 
            }
        });
    });
</script>