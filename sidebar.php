<div class="_aside myaccnt_aside">
    <h5 class="subHdng"><?php echo $nav_link_data[0]['my_account'];?></h5>
    <ul class="myaccnt_menu">
        <li><a href="myAccount.php" class="<?php if(@$page == 'profile') { echo 'active';}?>"><?php echo $nav_link_data[0]['profile'];?></a></li>
        <li><a href="booking-history.php" class="<?php if(@$page == 'history') { echo 'active';}?>"><?php echo $nav_link_data[0]['booking_history'];?></a></li>
        <li><a href="schedule-booking.php" class="<?php if(@$page == 'schedule') { echo 'active';}?>"><?php echo $nav_link_data[0]['schedule_booking'];?></a></li>
        <!-- <li><a href="refer-n-earn.php" class="<?php if(@$page == 'refer') { echo 'active';}?>">Refer &amp; Earn</a></li> -->
        <li><a href="change-password.php" class="<?php if(@$page == 'change-password') { echo 'active';}?>"><?php echo $nav_link_data[0]['change_password'];?></a></li>
        <li><a href="notifications.php" class="<?php if(@$page == 'notifications') { echo 'active';}?>"><?php echo $nav_link_data[0]['notifications'];?></a></li>
        <li><a href="discounts.php" class="<?php if(@$page == 'discounts') { echo 'active';}?>"><?php echo $nav_link_data[0]['discounts'];?></a></li>
        <li><a href="logout.php"><?php echo $nav_link_data[0]['logout'];?></a></li>
    </ul>
</div>