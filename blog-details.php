<?php include('header.php');

$blogs = $funcObject->blogsList($con);
$blog_details = $funcObject->blogDetails($con, $_GET['id']);

$blog_details = mysqli_fetch_assoc($blog_details);
//print_r($blog_details); die;
?>
    <div class="container-fluid">
        <div class="_header"></div>
        <!-- breadcrumb  -->
        <nav aria-label="breadcrumb" class="_custmBrdcrmb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
                <li class="breadcrumb-item active" aria-current="page">Hair Setting</li>
            </ol>
        </nav>
        <!-- <div id="preloader">
            <div id="status">&nbsp;</div>
        </div> -->
     
        <!-- page title -->
        <!--site-main start-->
        <div class="site-main">
            <!-- sidebar -->
            <div class="services-section clearfix">
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-12 content-area">
                            <!-- ttm-service-single-content-are -->
                            <article class="post ttm-blog-classic ttm-blog-single mb-0 clearfix">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div>
                                            <img class="img-fluid" src="<?php echo ADMIN_URL.$blog_details['image'];?>" alt="">
                                        </div>
                                        <!-- ttm-blog-classic-content -->
                                        <div class="ttm-blog-classic-content">
                                            <div class="ttm-post-entry-header">
                                                <div class="post-meta"> <span class="ttm-meta-line ttm-textcolor-skincolor"><time class="entry-date published" datetime="2018-07-28T00:39:29+00:00"><?php echo date('M d, Y', strtotime($blog_details['created_date']));?></time></span>
                                                    <span class="ttm-meta-line">By Alex</span>
                                                    <span class="ttm-meta-line"><a href="#">2 Comments</a></span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- ttm-blog-classic-content end -->
                                    </div>
                                    <div class="col-md-6 res-767-pt-15">
                                        <h2 class="entry-title"><blockquote class=""><h2 class="entry-title"><?php echo $blog_details['name'];?></h2>
                                        <?php echo $blog_details['short_desc'];?>
                                    </div>
                                </div>
                                <div class="row pt-30 res-767-p-0">
                                    <div class="col-md-12">
                                        <?php echo $blog_details['description'];?>
                                    </div>
                                </div>
                            
                            </article>
                            <!-- ttm-service-single-content-are end -->
                        </div>
                    </div>
                    <!-- row end -->
                </div>
            </div>
            <!-- sidebar end -->
        </div>
        <section id="feature-box" class="ttm-row beauty-service-section bg-img4 pb-10">
            <div class="beauty-service-title-wrapper">               
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7 col-md-7 col-sm-9 m-auto">
                            <div class="section-title with-desc text-center clearfix">
                                <div class="title-header">  
                                    <h5 class="ttm-textcolor-skincolor">Welcome</h5>
                                    <h2 class="title revealOnScroll" data-animation="fadeInLeft" data-timeout="400" >OUR OTHERS BLOGS</h2>
                                    <hr class="three-dot-hr" />
                                </div>
                             
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ttm-row bg-img1 contact-section res-767-p-15 clearfix mb-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="mrl-md-5rem">									
                                <div class="services2-slide owl-carousel mt_100 revealOnScroll" data-animation="fadeInDown" data-timeout="400" data-item="5" data-nav="true" data-dots="false" data-auto="false">
                                    <?php
                                    foreach($blogs as $row)
                                    {?>
                                        <div class="featured-imagebox featured-imagebox-post style3">
                                            <div class="ttm-post-thumbnail featured-thumbnail"> 
                                                <img class="img-fluid" src="<?php echo ADMIN_URL.$row['image'];?>" alt="image"> 
                                            </div>
                                            <div class="featured-content box-shadow featured-content-post">
                                                <div class="post-meta"> <span class="ttm-meta-line"><i class="fa fa-calendar"></i> <?php echo date('M d, Y', strtotime($row['created_date']));?></span>
                                                </div>
                                                <div class="featured-title">
                                                    <!-- featured-title -->
                                                    <h5><a href="blog-details.php?id=<?php echo $row['blog_id'];?>"><?php echo $row['name'];?></a></h5>
                                                </div>
                                                <div class="featured-desc">
                                                    <!-- featured-title -->
                                                    <p><?php echo substr($row['short_desc'], 0, 300).'...';?></p>
                                                </div>
                                                <div class="ttm-border-seperator"></div> <a class="ttm-btn ttm-btn-size-sm ttm-btn-color-skincolor btn-inline ttm-icon-btn-right mt-10" href="blog-details.php?id=<?php echo $row['blog_id'];?>">Read More <i class="ti ti-angle-double-right"></i></a>
                                            </div>
                                        </div>
                                    <?php
                                    }?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <a id="totop" href="#top"> <i class="fa fa-angle-up"></i>
        </a>    
    </div>

    <?php include('footer.php');?>