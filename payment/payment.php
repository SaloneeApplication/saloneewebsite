<?php
include('config.php');
include("../config/dbConnection.php");

$dbObject = new dbConnection();

$con = $dbObject->getConnection();

$user_id = $_POST['user_id'];
$slot_date = $_POST['slot_date'];
$slot_time = $_POST['slot_time'];
$order_id = time() . mt_rand() . $user_id;
$service_id = $_POST['service_id'];
$service_provider_id = $_POST['service_provider_id'];
$type = $_POST['type'];

$sql = "SELECT sale_price
    	FROM services_prices
    	WHERE service_provider_service_id = '$service_id' AND price_for = '$type'";
$recordSet = mysqli_query($con,$sql);

$data = array();
while($row = mysqli_fetch_array($recordSet))
{
	$total_amount = $row["sale_price"];
}

$slot_date = date('Y-m-d', strtotime($slot_date));
$slot_time = date('H:i:s', strtotime($slot_time));

$sql1 = "INSERT INTO service_slots(service_id, service_provider_id, user_id, total_amount, slot_date, service_type, order_id) 
		VALUES('$service_id', '$service_provider_id', '$user_id', '$total_amount', '$slot_date.$slot_time', '$type', '$order_id')";
$recordSet1 = mysqli_query($con,$sql1);
$slot_id = $con->insert_id;

?>
<html>
<head>
</head>
<body>
	<form method="post" name="customerData" id="customerData" action="ccavRequestHandler.php" style="display: none;">
		<table width="40%" height="100" border='1' align="center"><caption><font size="4" color="blue"><b>Integration Kit</b></font></caption></table>
			<table width="40%" height="100" border='1' align="center">
				<tr>
					<td>Parameter Name:</td><td>Parameter Value:</td>
				</tr>
				<tr>
					<td colspan="2"> Compulsory information</td>
				</tr>
				<tr>
					<td>Merchant Id	:</td><td><input type="text" name="merchant_id" value="<?php echo MERCHANT_ID; ?>"/></td>
				</tr>
				<tr>
					<td>Order Id	:</td><td><input type="text" name="order_id" value="<?php echo $order_id;?>"/></td>
				</tr>
				<tr>
					<td>Amount	:</td><td><input type="text" name="amount" value="<?php echo $total_amount;?>"/></td>
				</tr>
				<tr>
					<td>Currency	:</td><td><input type="text" name="currency" value="AED"/></td>
				</tr>
				<tr>
					<td>Redirect URL	:</td><td><input type="text" name="redirect_url" value="<?php echo REDIRECT_URL; ?>"/></td>
				</tr>
			 	<tr>
			 		<td>Cancel URL	:</td><td><input type="text" name="cancel_url" value="<?php echo CANCEL_URL; ?>"/></td>
			 	</tr>
			 	<tr>
					<td>Language	:</td><td><input type="text" name="language" value="EN"/></td>
				</tr>
		     	<tr>
		     		<td colspan="2">Billing information(optional):</td>
		     	</tr>
		        <tr>
		        	<td>Billing Name	:</td><td><input type="text" name="billing_name" value="Charli"/></td>
		        </tr>
		        <tr>
		        	<td>Billing Address	:</td><td><input type="text" name="billing_address" value="Room no 1101, near Railway station Ambad"/></td>
		        </tr>
		        <tr>
		        	<td>Billing City	:</td><td><input type="text" name="billing_city" value="Indore"/></td>
		        </tr>
		        <tr>
		        	<td>Billing State	:</td><td><input type="text" name="billing_state" value="MP"/></td>
		        </tr>
		        <tr>
		        	<td>Billing Zip	:</td><td><input type="text" name="billing_zip" value="425001"/></td>
		        </tr>
		        <tr>
		        	<td>Billing Country	:</td><td><input type="text" name="billing_country" value="India"/></td>
		        </tr>
		        <tr>
		        	<td>Billing Tel	:</td><td><input type="text" name="billing_tel" value="9595226054"/></td>
		        </tr>
		        <tr>
		        	<td>Billing Email	:</td><td><input type="text" name="billing_email" value="atul.kadam@avenues.info"/></td>
		        </tr>
		        <tr>
		        	<td colspan="2">Shipping information(optional)</td>
		        </tr>
		        <tr>
		        	<td>Shipping Name	:</td><td><input type="text" name="delivery_name" value="Chaplin"/></td>
		        </tr>
		        <tr>
		        	<td>Shipping Address	:</td><td><input type="text" name="delivery_address" value="room no.701 near bus stand"/></td>
		        </tr>
		        <tr>
		        	<td>shipping City	:</td><td><input type="text" name="delivery_city" value="Hyderabad"/></td>
		        </tr>
		        <tr>
		        	<td>shipping State	:</td><td><input type="text" name="delivery_state" value="Andhra"/></td>
		        </tr>
		        <tr>
		        	<td>shipping Zip	:</td><td><input type="text" name="delivery_zip" value="425001"/></td>
		        </tr>
		        <tr>
		        	<td>shipping Country	:</td><td><input type="text" name="delivery_country" value="India"/></td>
		        </tr>
		        <tr>
		        	<td>Shipping Tel	:</td><td><input type="text" name="delivery_tel" value="9595226054"/></td>
		        </tr>
		        <tr>
		        	<td>Slot ID	:</td><td><input type="text" name="merchant_param1" value="<?php echo $slot_id;?>"/></td>
		        </tr>
		        <tr>
		        	<td>User ID	:</td><td><input type="text" name="merchant_param2" value="<?php echo $user_id;?>"/></td>
		        </tr>
				<tr>
					<td>Promo Code	:</td><td><input type="text" name="promo_code" value=""/></td>
				</tr>
				<tr>
					<td>Vault Info.	:</td><td><input type="text" name="customer_identifier" value=""/></td>
				</tr>
		        <tr>
		        	<td></td><td><INPUT TYPE="submit" value="CheckOut"></td>
		        </tr>
	      	</table>
	      </form>
	</body>
</html>
<script type="text/javascript">
	document.forms["customerData"].submit();
</script>