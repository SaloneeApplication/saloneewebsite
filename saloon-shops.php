<?php include('header.php');

$shop_list = $funcObject->shopList($con);

?>
<div class="container-fluid">
    <div class="_header"></div>
    <nav aria-label="breadcrumb" class="_custmBrdcrmb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
            <li class="breadcrumb-item active" aria-current="page">Salon</li>
        </ol>
    </nav>

    <div class="nearShops">
        <ul>
            <?php
            foreach($shop_list as $row)
            {?>
                <li class="revealOnScroll" data-animation="fadeInDown" data-timeout="300" style="margin-bottom: 10px;">
                    <div class="card" style="width: 17rem; height: 500px; overflow: auto;">
                        <div class="imgOuter">
                            <img class="card-img-top" src="<?php echo ADMIN_URL.$row['image'];?>" alt="a">
                        </div>
                        <div class="card-body ">
                            <h5 class="card-title"><?php echo $row['business_name'];?></h5>
                            <div class="threeFive">
                                <div class="stars-outer">
                                    <div class="stars-inner" style="width: 70%;"></div>
                                </div>
                                <span class="number-rating">3.5</span>
                              </div>
                            <p><i><img src="assets/img/icons/loc.png" alt="loc" /></i><span><?php echo $row['address'];?></span></p>
                            <p><i><img src="assets/img/icons/clock.png" alt="clock" /></i><span>Opens at 10:00 AM</span>
                            </p>
                            <a href="shop-details.php?id=<?php echo $row['service_provider_id'];?>" class="_btn book">View</a>
                        </div>
                    </div>
                </li>
            <?php
            }?>
        </ul>
        <div class="text-center">
            <a href="javascript:void(0)" class="btnViewMore">View More</a>
        </div>
    </div>
</div>
 <?php include('footer.php');?>
