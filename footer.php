<footer class="footer widget-footer clearfix">
    <div class="first-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-7 col-sm-9 p-5 m-auto text-center">                            
                    <h2 class="text-start color-pink font-weight-bold">CHOOSE FROM THE BEST</h2>                                                  
                </div>

            </div>

            <div class="row border-pink" >
                <div class="col-lg-3 m-auto">
                    <img id="footer-logo-img" class="img-center" src="assets/img/small-logo.png" alt="">
                </div>
                <div class="col-lg-9"><h3 class="color-pink font-weight-bold">Book Appointment Online</h3>
                <p class="ttm-textcolor-white">Locate a salon that is near you and book appointment with just one click with ease and seamlessly. Schedule, reschedule or cancel your appointments on your mobile phones. </p>
                 
                </div>
            </div>


        </div>
    </div>
    <div class="ttm-textcolor-white">
        <div class="container">
            <div class="row border-pink">
                <div class="col">
                	<h4 class="color-pink font-weight-bold">QUICK LINKS</h4>
                </div>
                <div class="col">
                <ul class="ttm-list ttm-list-style-icon ttm-list-icon-color-skincolor style3">
                    <li><a href="index.php#aboutus-box"><?php echo $nav_link_data[0]['about_us'];?></a></li>
                    <li><a href="#"><?php echo $nav_link_data[0]['how_it_works'];?></a></li>
                    <li><a href="index.php#download-box"><?php echo $nav_link_data[0]['download'];?></a></li>
                    <li><a href="#"><?php echo $nav_link_data[0]['contact'];?></a></li>
                </ul>
            </div>
                <div class="col">
                    <ul class="ttm-list ttm-list-style-icon ttm-list-icon-color-skincolor style3">
                        <li><a href="/partner.php"><?php echo $nav_link_data[0]['partner_with_salonee'];?></a></li>
                        <li><a href="#"><?php echo $nav_link_data[0]['privacy_policy'];?></a></li>
                        <li><a href="#"><?php echo $nav_link_data[0]['terms_conditions'];?></a></li>
                        <li><a href="#"><?php echo $nav_link_data[0]['faq'];?></a></li>
                    </ul>
                </div>
                <div class="col">
                    <p>
                    Support <br>
                    <a href="mailto:info@salonee.com">info@salonee.com</a>
                    <br>
                    Copyright ©2020 All rights reserved.
                    </p>                      
                </div>
            </div>
             
        </div>
    </div>
    <div class="">
        <div class="container">
            <div class="row copyright">
                <div class="col-md-6">
                    <div class="widget widget_text clearfix justify-content-end social-icons" align="left">
	                    <ul class="social-icons list-inline">
							<li><a target="_blank" href="https://www.facebook.com/Salonee-UAE-101914768550451" class="tooltip-top" data-tooltip="Facebook"><i style="font-size: 25px;" class="fa fa-facebook"></i></a></li>
							<li><a target="_blank" href="https://twitter.com/SaloneeUae" class="tooltip-top" data-tooltip="Twitter"><i style="font-size: 25px;" class="fa fa-twitter"></i></a></li>
							<li><a target="_blank" href="https://www.instagram.com/mysaloneeuae/" class="tooltip-top" data-tooltip="Instagram"><i style="font-size: 25px;" class="fa fa-instagram"></i></a></li>
							<li><a target="_blank" href="https://www.youtube.com/channel/UCpg_nrx5bBtyewCkR9ueebQ" class="tooltip-top" data-tooltip="Youtube"><i style="font-size: 25px;" class="fa fa-youtube"></i></a></li>
							<li><a target="_blank" href="#" class="tooltip-top" data-tooltip="Snapchat"><i style="font-size: 25px;" class="fa fa-snapchat"></i></a></li>

						</ul>
	                </div>
                </div>
                <div class="col-md-6">
                    <div class="pt-4 pb-4" align="right">
                        <img src="assets/img/power-by.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<a id="totop" href="#top">
<i class="fa fa-angle-up"></i>
</a>
<!-- <div class="float-sm">
	<div class="fl-fl float-tw"> <i class="fa fa-twitter" aria-hidden="true"></i>
		<a href="" target="_blank">Follow us!</a>
	</div>
	<div class="fl-fl float-gp"> <i class="fa fa-google-plus"></i>
		<a href="" target="_blank">Recommend us!</a>
	</div>
	<div class="fl-fl float-rs"> <i class="fa fa-rss"></i>
		<a href="" target="_blank">Follow via RSS</a>
	</div>
	<div class="fl-fl float-ig"> <i class="fa fa-instagram"></i>
		<a href="" target="_blank">Follow us!</a>
	</div>
	<div class="fl-fl float-pn"> <i class="fa fa-pinterest"></i>
		<a href="" target="_blank">Follow us!</a>
	</div>
	<div class="fl-fl float-pn"> <i class="fa fa-facebook"></i>
		<a href="" target="_blank"> Like us!</a>
	</div>
</div> -->
</div>
<!--+++++++++++++++++++++++++ modal section ++++++++++++++++++++++++++++++  -->
<!-- location modal  -->
<div class="modal fade themeModal" id="locationModal" tabindex="-1" role="dialog" aria-labelledby="locationModalTitle" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="col-12 modal-title text-center" id="locationModalTitle">
				Choose Your Location
			</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="login">
				<form>
					<div class="form-group mt-4">
						<input type="text" id="search-box" name="city_id" class="form-control" autocomplete="off" required/>
						<label class="form-control-placeholder p-0" for="search-box">Enter City Name</label>
						<div id="suggesstion-box"></div>
					</div>
					<div class="form-group mt-4">
						<button style="background: #424041;color: #fff;" type="button" class="btn theme-btn"><i class="fa fa-map-marker"></i> Use my current location</button> 
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>
<!-- login modal  -->
<div
class="modal fade themeModal lognModal"
id="loginModal"
tabindex="-1"
role="dialog"
aria-labelledby="loginModalTitle"
aria-hidden="true"
>
<div class="modal-dialog modal-dialog-centered" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="col-12 modal-title text-center" id="loginModalTitle">
				<img
					src="./assets/img/salonee-logo-small-new.png"
					alt="logo"
					class="modal_logo"
					/>
			</h5>
			<button
				type="button"
				class="close"
				data-dismiss="modal"
				aria-label="Close"
				>
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="login">
				<form id="login_form" method="POST">
					<div class="form-group">
						<input type="text" id="username" name="username" class="form-control" autocomplete="off" required/>
						<label class="form-control-placeholder p-0" for="username">User Name</label>
					</div>
					<div class="form-group mb_1">
						<input type="password" id="password" name="password" class="form-control" autocomplete="off" required/>
						<label class="form-control-placeholder p-0" for="password">Password</label>
						<span toggle="#password" class="fa fa-eye field-icon toggle-password"></span>
					</div>
					<p id="forgot_pswd">
						<a href="#" id="forgotModalPopup"  data-toggle="modal" data-target="#fpModal"
							>Forgot Password ?</a
							>
					</p>
					<div class="form-group mb_1">
						<button type="button" class="btn login-btn" id="post_login">Login</button>
					</div>
					<div class="form-group mb_1 social-icons">
						<p class="sign-Up">
							Don't Have an Account
							<a href="#" id="signupBtn" data-toggle="modal" data-target="#signupModal">SIGN UP</a>
						</p>
						<p class="sign-Up">
							Click here, If you are interested to join with us.
							<a href="https://mysalonee.com/serviceprovider/register.php" target="_blank" id="signupBtn">JOIN</a>
						</p>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>
<!-- sgnup modal -->
<div
class="modal fade themeModal lognModal"
id="signupModal"
tabindex="-1"
role="dialog"
aria-labelledby="signupModalTitle"
aria-hidden="true"
>
<div class="modal-dialog modal-dialog-centered" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="col-12 modal-title text-center" id="locationModalTitle">
				Register
			</h5>
			<button
				type="button"
				class="close"
				data-dismiss="modal"
				aria-label="Close"
				>
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="login">
				<form id="signupForm" method="POST">
					<div class="form-group">
						<input
							type="text"
							id="full_name"
							name="full_name"
							class="form-control"
							autocomplete="off"
							required
							/>
						<label class="form-control-placeholder p-0" for="full_name">Full Name</label>
					</div>
                    <div class="form-row">
						<div class="form-group col-md-6">
							<select class="form-control" autocomplete="off" name="mobile_code" id="mobile_code" required>
	                            <option hidden disabled selected value>
									- Country Code -
								</option>
	                            <option value="91">India (91 )‎</option>
	                            <option value="971">United Arab Emirates (971 )‎</option>                                     
	                        </select>
							<!-- <label class="form-control-placeholder p-0" for="mobile_code">Enter Country Code</label> -->
						</div>
						<div class="form-group col-md-6">
							<input type="text" id="mobile_number" name="mobile_number" class="form-control" autocomplete="off" required="" aria-required="true">
							<label class="form-control-placeholder p-0" for="mobile_number">Enter Mobile Number</label>
						</div>
					</div>
					<div class="form-group">
						<input type="text" id="email" name="email" class="form-control" autocomplete="off" required />
						<label class="form-control-placeholder p-0" for="email">Email Id</label>
					</div>
					<div class="form-group">
						<input type="password" id="password-field" name="password" class="form-control" autocomplete="off" required/>
						<label class="form-control-placeholder p-0" for="password-field">Password</label>
						<span toggle="#password-field" class="fa fa-eye field-icon toggle-password"></span>
					</div>
					<div class="form-group">
						<select name="gender" class="form-control" autocomplete="off" id="gender" required>
							<option hidden disabled selected value>
								- Select gender -
							</option>
							<option value="male">Male</option>
							<option value="female">Female</option>
							<option value="other">Other</option>
						</select>
						<!-- <label class="form-control-placeholder p-0" for="gender">gender</label> -->
					</div>
					<div class="form-group">
						<input type="text" id="area" name="area" class="form-control" autocomplete="off" required/>
						<label class="form-control-placeholder p-0" for="area">Area</label>
					</div>
					<div class="form-group mb_1">
						<button type="button" class="btn login-btn" id="signUpButton">Submit</button>
					</div>
					<p class="sign-Up">
						Click here, If you are interested to join with us.
						<a href="https://mysalonee.com/serviceprovider/register.php" target="_blank" id="signupBtn">JOIN</a>
					</p>
				</form>
			</div>
		</div>
	</div>
</div>
</div>
<!-- forgot password -->
<div
class="modal fade themeModal lognModal"
id="fpModal"
tabindex="-1"
role="dialog"
aria-labelledby="fpModalTitle"
aria-hidden="true"
>
<div class="modal-dialog modal-dialog-centered" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="col-12 modal-title text-center" id="fpModalTitle">
				Forgot Password
			</h5>
			<button
				type="button"
				class="close"
				data-dismiss="modal"
				aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="login">
				<form id="forgotPaswdForm">
					<div class="form-group">
						<input
							type="text"
							id="email_id"
							name="email"
							class="form-control"
							autocomplete="off"
							required />
						<label class="form-control-placeholder p-0" for="email_id">Enter Email ID</label>
					</div>
					<div class="form-group mb_1">
						<button class="btn login-btn" id="post_fg" type="button">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>
<!-- Social Login popup -->
<div class="modal fade themeModal lognModal socialLoginModal" id="socialLoginModal" tabindex="-1" role="dialog" aria-labelledby="slModalTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="col-12 modal-title text-center" id="slModalTitle">
					Please Provide your Mobile Number to Sign Up
				</h5>
				<button
					type="button"
					class="close"
					data-dismiss="modal"
					aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="login">
					<form id="otpReqForm" >
						<p class="align-center f-12"> We will send you one time password (OTP)</p>
						<div class="form-group">               
							<input id="phone" type="tel" class="form-control">
							<span id="valid-msg" class="hide"><img src="assets/img/icons/checkmark.svg" /> </span>
							<span id="error-msg" class="hide">Invalid number</span> 
						</div>
						<div class="form-group mb_1">
							<button class="btn login-btn" id="getOTPBtn" onclick="showOTPform();return false;">GET OTP</button>
						</div>
					</form>
					<div id="OTPform" class="hide">
						<p class="align-center f-12"> We have sent the OTP to +50 1234567</p>
						<div class="Flx_row">
							<input type="text" class="otpInput" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
							<input type="text" class="otpInput" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
							<input type="text" class="otpInput" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
							<input type="text" class="otpInput" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
						</div>
						<p>If you didn't receive OTP! <a href="#">Resend</a></p>
						<button class="btn login-btn" onclick="showotpReqForm();return false;">Verify & Proceed</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- otp modal -->
<div class="modal fade themeModal lognModal otpmodal" id="otpmodal" tabindex="-1" role="dialog" aria-labelledby="slModalTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="col-12 modal-title text-center" id="slModalTitle">
					Verify OTP
				</h5>
				<button
					type="button"
					class="close"
					data-dismiss="modal"
					aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="login">
					<form id="otpReqForm" >
						<div id="OTPform">
							<input type="hidden" id="user_id">
							<p class="align-center f-12"> We have sent the OTP for verification</p>
							<div class="Flx_row">
								<input type="text" id="otp1" class="otpInput" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
								<input type="text" id="otp2" class="otpInput" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
								<input type="text" id="otp3" class="otpInput" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
								<input type="text" id="otp4" class="otpInput" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
							</div>
							<p>If you didn't receive OTP! <a href="#">Resend</a></p>
							<button class="btn login-btn" id="otp_verify" onclick="showotpReqForm();return false;">Verify & Proceed</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
	<div id="coordinates" style="display: none;"></div>
	<?php 
	if(@$_SESSION['lang'] == 'Arabic')
	{?>
		<script src="assets/js-rtl/jquery.min.js"></script>
		<script src="assets/js-rtl/bootstrap.min.js"></script>
		<script src="./assets/js-rtl/bootstrap.bundle.min.js"></script>
		<script src="./assets/js-rtl/jquery.easing.js"></script>    
		<script src="assets/js-rtl/owl.carousel.js"></script> 
		<script src="./assets/js-rtl/bootstrap-select.min.js"></script>
		<script src="assets/js-rtl/jquery.prettyPhoto.js"></script>
		<script src="assets/revolution-rtl/js/revolution.tools.min.js"></script>
		<script src="assets/revolution-rtl/js/rs6.min.js"></script>
		<script src="assets/revolution-rtl/js/slider.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
		<script src="assets/js-rtl/main.js"></script>
	<?php
	}else
	{?>
		<script src="assets/js/jquery-3.3.1.min.js"></script>
		<script src="assets/bootstrap-4.3.1/js/bootstrap.min.js"></script>
		<script src="./assets/js/js/bootstrap.bundle.min.js"></script>
		<script src="./assets/js/js/jquery.easing.js"></script>    
		<script src="assets/js/owl.carousel.js"></script> 
		<script src="./assets/js/js/bootstrap-select.min.js"></script>
		<script src="assets/js/js/jquery.prettyPhoto.js"></script>
		<script src="assets/revolution/js/revolution.tools.min.js"></script>
		<script src="assets/revolution/js/rs6.min.js"></script>
		<script src="assets/revolution/js/slider.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
		<script src="assets/js/js/main.js"></script>
	<?php
	}?>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" ></script>
	<!-- countryflag with mobile number validation  -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/intlTelInput.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/intlTelInput.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js" integrity="sha512-Zq9o+E00xhhR/7vJ49mxFNJ0KQw1E1TMWkPTxrWcnpfEFDEXgUiwJHIKit93EW/XxE31HSI5GEOW06G6BF1AtA==" crossorigin="anonymous"></script>
	<script src="assets/js/sweetalert2.all.min.js"></script>
	<script src="assets/js/main.js"></script>

	<!-- Form Validation -->
	<script src="assets/js/jquery.validate.js"></script> 
	<!-- <script async src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDT-Q2gPi2BlE5yGPIKyaYDzAmh9AtQ0bw&libraries=places&callback=initMap"></script> -->
	</body>
</html>

<script type="text/javascript">
$(document).ready(function(){
	$("#search-box").keyup(function(){
		$.ajax({
			type: "POST",
			url: "controllers/ajax_city_search.php",
			data:'keyword='+$(this).val(),
			success: function(data){
				$("#suggesstion-box").show();
				$("#suggesstion-box").html(data);
				$("#search-box").css("background","#FFF");
			}
		});
	});
});

//To select city name
function get_city(id, val) {
	
	$.ajax({
		type: "POST",
		url: "controllers/city_session.php",
		data: 'city_id='+id+'&city_name='+val,
		success: function(res){
			$("#search-box").val(val);
			$("#suggesstion-box").hide();
			location.reload()
		}
	});
}

$(document).ready(function(){
	$("#search-input").keyup(function(){
		$.ajax({
			type: "POST",
			url: "controllers/ajax_search.php",
			data:'keyword='+$(this).val(),
			success: function(data){
				$("#suggesstion-area").show();
				$("#suggesstion-area").html(data);
			}
		});
	});
});
</script>

<script>
	$(document).ready(function(){
		getLocation();
	})

	var x = $('#coordinates')

	function getLocation() {
	  	if (navigator.geolocation) {
	    	navigator.geolocation.getCurrentPosition(showPosition);
	  	} else { 
	    	x.innerHTML = "Geolocation is not supported by this browser.";
	  	}
	}

	function showPosition(position) {
		x.innerHTML = "Latitude: " + position.coords.latitude + 
		"<br>Longitude: " + position.coords.longitude;
	}
</script>

<script>

	/*function initialize() {
		var input = document.getElementById('area');
		new google.maps.places.Autocomplete(input);
	}

	google.maps.event.addDomListener(window, 'load', initialize);*/

	$(".toggleTopbarItems").click(function(){
		$(".topBarItems").toggle();
		$(this).toggleClass('rotate')
	});
	
	$(document).ready(function(){
		$(function () {
			$('.selectpicker').selectpicker();
		});
		
		
		// =====  services-slide ==== 
		$(".services-slide").owlCarousel({ 
		  margin: 0,
		  loop:true,
		  nav: $('.services-slide').data('nav'),
		  dots: $('.services-slide').data('dots'),                     
		  autoplay: $('.services-slide').data('auto'),
		  smartSpeed: 3000,
		  responsive:{
		      0:{
		          items:1
		      },
		
		      680:{
		          items:2
		      },
		      992:{
		          items: 4
		      },
		1366:{
		          items: 4
		      },
		1400:{
		          items: $('.services-slide').data('item')
		      }
		  }    
		});
		
		/*------------------------------------------------------------------------------*/
		/* Page Animations
		/*------------------------------------------------------------------------------*/
		
		$(function() {
		
			var $window           = $(window),
			win_height_padded = $window.height() * 1.1,
			isTouch           = Modernizr.touch;
			
			if (isTouch) { $('.revealOnScroll').addClass('animated'); }
			
			$window.on('scroll', revealOnScroll);
			
			function revealOnScroll() {
				var scrolled = $window.scrollTop(),
				 win_height_padded = $window.height() * 1.1;
				
				// Showed...
				$(".revealOnScroll:not(.animated)").each(function () {
					var $this     = $(this),
					offsetTop = $this.offset().top;
					
					if (scrolled + win_height_padded > offsetTop) {
						if ($this.data('timeout')) {
							window.setTimeout(function(){
								$this.addClass('animated ' + $this.data('animation'));
							}, parseInt($this.data('timeout'),10));
						} else {
							$this.addClass('animated ' + $this.data('animation'));
						}
					}
				});
				// Hidden...
				$(".revealOnScroll.animated").each(function (index) {
					var $this     = $(this),
					offsetTop = $this.offset().top;
					if (scrolled + win_height_padded < offsetTop) {
						$(this).removeClass('animated fadeInUp fadeInDown flipInX lightSpeedIn fadeInLeft fadeInRight'),
						$(this).removeClass('animated flipInY lightSpeedIn bounceIn zoomIn zoomInUp')
					}
				});
			}
			
			revealOnScroll();
		});	
	});
	
	// TOGGLE PASSWORD
	$(".toggle-password").click(function () {
	  	$(this).toggleClass("fa-eye fa-eye-slash");
	  	var input = $($(this).attr("toggle"));
	  	if (input.attr("type") == "password") {
	    	input.attr("type", "text");
	  	} else {
	    	input.attr("type", "password");
	  	}
	});
	
    // SIGNUP FORM SUBMIT
    $("#signUpButton").click(function () {  
    	if ($("#signupForm").valid()) {

    		var full_name = $("#full_name").val();
    		var email = $("#email").val();
    		var mobile_code = $("#mobile_code").val();
    		var mobile_number = $("#mobile_number").val();
    		var password = $("#password-field").val();
    		var gender = $("#gender").val();
    		var area = $("#area").val();

    		var formData = new FormData();

    		formData.append('full_name', full_name);
    		formData.append('email', email);
    		formData.append('mobile_code', mobile_code);
    		formData.append('mobile_number', mobile_number);
    		formData.append('password', password);
    		formData.append('gender', gender);
    		formData.append('area', area);

		    $.ajax({
                type:'POST',
                url:'controllers/registration.php',
                data:formData,
                contentType: false,
			    processData: false,
                success:function(res){
					if(res > 0)
					{
						/*swal({
							type: "success",
							text: 'Registration Successful',
							showConfirmButton: false,
							timer: 1500
						});*/
						//setInterval('location.reload()', 1500); 

						$('#signupModal').modal('hide');
						$("#user_id").val(res);
						$('#otpmodal').modal('show');
					}
					else
					{
						swal({
							type: "error",
							text: 'Something went wrong',
							showConfirmButton: false,
							timer: 1500
						});
					}			                    
                }
            }); 
		}
	});

	$('#otp_verify').click(function () {

		var user_id = $("#user_id").val();
		var otp1 = $("#otp1").val();
		var otp2 = $("#otp2").val();
		var otp3 = $("#otp3").val();
		var otp4 = $("#otp4").val();

		var formData = new FormData();

    	formData.append('user_id', user_id);
    	formData.append('otp1', otp1);
    	formData.append('otp2', otp2);
    	formData.append('otp3', otp3);
    	formData.append('otp4', otp4);

		$.ajax({
            type:'POST',
            url:'controllers/verify_otp.php',
            data:formData,
            contentType: false,
		    processData: false,
            success:function(res){
				if(res == 1)
				{
					swal({
						type: "success",
						text: 'Registration Successful',
						showConfirmButton: false,
						timer: 1500
					});
					setInterval('location.reload()', 1500); 
				}
				else
				{
					swal({
						type: "error",
						text: res,
						showConfirmButton: false,
						timer: 1500
					});
				}			                    
            }
        });
	});
	
    // LOGIN FORM SUBMIT
    $("#post_login").click(function () {
      	if ($("#login_form").valid()) {
        	var username = $("#username").val();
    		var password = $("#password").val();

    		var formData = new FormData();

    		formData.append('username', username);
    		formData.append('password', password);

		    $.ajax({
                type:'POST',
                url:'controllers/login.php',
                data:formData,
                contentType: false,
			    processData: false,
			    dataType:'json',
                success:function(res){

					if(res == 1)
					{
						swal({
							type: "success",
							text: 'Login Successfull',
							showConfirmButton: true,
							timer: 1500
						});
						setInterval('location.reload()', 1500); 
					}
					else
					{
						swal({
							type: "error",
							text: 'Incorrect Username or Password'
						});
						return false;
					}	                    
                }
            }); 
		}
    });

    // SIGNUP FORM VALIDATIONS
    $("#signupForm").validate({
      // Specify the validation rules
      rules: {
        full_name: {
          required: true,
          minlength: 3,
          maxlength: 50,
        },
        email: {
          required: true,
          email: true,
        },
        mobile_code: {
          required: true,
        },
      	mobile_number:{
      		required: true,
          	digits: true,
      	},
        password: {
          required: true,
          minlength: 8,
          maxlength: 15,
        },
        gender: {
          required: true,
        },
        area: {
          required: true,
        },
        
      },
      // Specify the validation error messages
      messages: {
        full_name: {
          required: "Please Enter Full Name",
          minlength: "Full Name must be minimum 3 characters",
          maxlength: "Maximum 50 characters only",
        },
        email: {
          required: "Please Enter Email Address",
          email: "Please Enter a Valid Email Address",
        },
      
        password: {
          required: "Please Enter Password",
          minlength: "Password must be minimum 8 characters",
          maxlength: "Maximum 15 characters only",
        },
        gender: {
          required: "Please Select gender",
        },
        area: {
          required: "Please Enter Location",
        },
       
      },
    });

    // LOGIN FORM VALIDATIONS
    $("#login_form").validate({
      // Specify the validation rules
      rules: {
        NAME: {
          required: true,
          minlength: 3,
          maxlength: 50,
        },
        PASSWORD: {
          required: true,
          minlength: 8,
          maxlength: 15,
        },
      },
      // Specify the validation error messages
      messages: {
        NAME: {
          required: "Please Enter User Name",
          minlength: "User Name must be minimum 3 characters",
          maxlength: "Maximum 50 characters only",
        },

        PASSWORD: {
          required: "Please Enter Password",
          minlength: "Password must be minimum 8 characters",
          maxlength: "Maximum 15 characters only",
        },
      },
    });

    // FORGOTPASSWORD FORM VALIDATIONS
    $("#forgotPaswdForm").validate({
      rules: {
        forgotPaswdForm: {
          required: true,
          email: true,
        },
      },
      messages: {
        fpswdmobleNmbr: {
          required: "Please enter email"
        },
      },
    });

    $("#post_fg").click(function () {
      	if ($("#forgotPaswdForm").valid()) {
        	var email = $("#email_id").val();

    		var formData = new FormData();

    		formData.append('email', email);

		    $.ajax({
                type:'POST',
                url:'controllers/forgot_password.php',
                data:formData,
                contentType: false,
			    processData: false,
			    dataType:'json',
                success:function(res){

					if(res == 1)
					{
						swal({
							type: "success",
							text: 'Your password has been sent successfully to registred email id',
							showConfirmButton: true,
							timer: 1500
						});
						location.href = 'index.php';
					}
					else
					{
						swal({
							type: "error",
							text: 'Incorrect Email'
						});
						return false;
					}	                    
                }
            }); 
		}
    });

    $("#subscribe-form").validate({
      	// Specify the validation rules
        rules: {
        subscribe_email: {
          required: true,
          // remote:'../controllers/subscription.php',
          email: true,
        },
        },
     	// Specify the validation error messages
    	messages: {
    	subscribe_email: {
          required: "Please Enter Email Address",
          email: "Please Enter a Valid Email Address",
          // remote:"Already subscribed"
        },
        },
    });

    $('#subscribe').click(function(e) {
	    e.preventDefault();
    	if ($("#subscribe-form").valid()) {
		var email = $("#subscribe_email").val();

		var formData = new FormData();
    	formData.append('email', email);
	    $.ajax({
            type:'POST',
            url:'controllers/subscription.php',
            data:formData,
            contentType: false,
		    processData: false,
		    dataType:'json',
            success:function(res){
			$('#subscribe_email').val("");
				if(res.status == 1)
				{
					swal({
						type: "success",
						text: res.message,
						showConfirmButton: true,
						timer: 1500
					});
				} else if(res.status == 2){
					swal({
						type: "error",
						text: res.message,
						timer: 1800
					});
				} else if(res.status == 3){
					swal({
						type: "error",
						text: res.message,
						timer: 1800
					});
				}	                    
            }
        }); 
	}
    })

    //hidelogin while clicking on forgot
    $('#forgotModalPopup').click(function(){
      $('#loginModal').modal('hide');
    });
    $('#socialLogin').click(function(){
      $('#loginModal').modal('hide');
    });
    $('#socialLogin1').click(function(){
      $('#loginModal').modal('hide');
    });
    $('#signupBtn').click(function(){
      $('#loginModal').modal('hide');
    });


    function showOTPform(){
       $('#otpReqForm').hide();
       $('#OTPform').removeClass('hide');        
    }

    function showotpReqForm(){
      $('#otpReqForm').show();  
      $('#OTPform').addClass('hide')
    }

    function lang_session(lang)
    {
    	var url = "lang_session.php";
		$.post(url,{lang:lang},function(data){
			location.reload();
		});
    }

    $('.app_link').click(function(){
    	swal({
			icon: 'info',
			text: "Coming Soon",
			timer: 1500
		});
    })
</script>