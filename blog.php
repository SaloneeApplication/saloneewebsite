<?php include('header.php');

$blogs = $funcObject->blogsList($con);
 ?>
    <div class="container-fluid">
        <div class="_header"></div>
        <!-- breadcrumb  -->
        <nav aria-label="breadcrumb" class="_custmBrdcrmb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Blogs</li>
            </ol>
        </nav>
        <!-- <div id="preloader">
            <div id="status">&nbsp;</div>
        </div> -->
        <section id="feature-box" class="ttm-row beauty-service-section bg-img4 pb-10">
            <div class="ttm-row bg-img1 contact-section res-767-p-15 clearfix mb-0">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="mrl-md-5rem">	
                            	<div class="row">							
                                    <?php
                                    foreach($blogs as $row)
                                    {?>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="row featured-imagebox featured-imagebox-post style3 revealOnScroll" data-animation="fadeInDown" data-timeout="200">
                                                <div class="col-md-7 ttm-post-thumbnail featured-thumbnail p-0">
                                                    <img class="img-fluid" src="<?php echo ADMIN_URL.$row['image'];?>" alt="image">
                                                </div>
                                                <div class="col-md-5 featured-content featured-content-post">
                                                    <div class="post-meta"> <span class="ttm-meta-line"><i class="fa fa-calendar"></i><?php echo date('M d, Y', strtotime($row['created_date']));?></span>
                                                    </div>
                                                    <div class="post-title featured-title">
                                                        <h5><a href="blog-details.php?id=<?php echo $row['blog_id'];?>"><?php echo $row['name'];?></a></h5>
                                                    </div>
                                                    <div class="featured-desc">
                                                        <p><?php echo substr($row['short_desc'], 0, 300).'...';?></p>
                                                    </div>
                                                    <div class="post-footer"> <span class="ttm-meta-line"><a class="ttm-textcolor-skincolor" href="blog-details.php?id=<?php echo $row['blog_id'];?>">read more&nbsp;&nbsp;<i class="fa fa-angle-right"></i></a></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                    }?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <a id="totop" href="#top"> <i class="fa fa-angle-up"></i>
        </a>
      
    </div>

    <?php include('footer.php');?>