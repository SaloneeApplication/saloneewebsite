<?php 
include('header.php');

$category_id = @$_GET['category_id'];
$sp_id = @$_GET['sp_id'];
$services = $funcObject->servicesByCategory($con, @$category_id, @$sp_id);
$categories = $funcObject->categoriesList($con);

?>

<style type="text/css">
.img_container{
    display: inline-block;
    font-size: 40px;
    line-height: 50px;
    color:#c96c92;
    width: 50px;
    height: 50px;
    text-align: center;
    vertical-align: bottom;
    left: 50px;
    margin-left: 45%;
    margin-top: 10%;
}
</style>

<link id="bsdp-css" href="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css" rel="stylesheet">
<div class="container-fluid">
    <div class="_header"></div>
    <!-- breadcrumb  -->
    <nav aria-label="breadcrumb" class="_custmBrdcrmb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <!-- <li class="breadcrumb-item"><a href="#">Library</a></li> -->
            <li class="breadcrumb-item active" aria-current="page">Hair Setting</li>
        </ol>
    </nav>

    <div class="d-flex myFlex">
        <div class="_aside">
            <h5 class="subHdng">Categories</h5>
            <div class="mt-4 custom-checkbox">
            <?php
            while($row = mysqli_fetch_array($categories))
            {?>
                <div class="form-check mt-2 ">
                    <?php $checked = (@$_GET['category_id'] == $row['category_id'] ? 'checked' : ''); ?>
                    <input type="checkbox" name="category_id" class="form-check-input category_id" <?= $checked;?> value="<?= $row['category_id']?>" id="customCheck<?= $row['category_id']?>">
                    <label class="form-check-label" for="customCheck<?= $row['category_id']?>"> <?= $row['name'].' ('.ucfirst($row['category_for']).')';?></label>
                </div>
            <?php
            }?>
            </div>
        </div>

        <div class="mainDiv cat-data">
            <?php
            while($row = mysqli_fetch_array($services))
            {?>
                <div class="card " style="width: 18rem;">
                    <div class="imgOuter">
                        <img class="card-img-top revealOnScroll" data-animation="flipInY" data-timeout="400" src="<?php echo $row['image'];?>" alt="messy hair look">
                    </div>
                    <div class="card-body">
                        <input type="hidden" name="service_at" value="<?php echo $row['service_at'];?>">
                        <input type="hidden" name="service_id" value="<?php echo $row['service_id'];?>">
                        <input type="hidden" name="service_provider_id" value="<?php echo $row['service_provider_id'];?>">
                        <h5 class="card-title"><?php echo $row['name'];?></h5>
                        <h6><?php echo $row['service_provider_name'];?></h6>
                        <p class="card-text"><?php echo $row['description'];?></p>
                        <a href="#" data-toggle="modal" data-target="#bookModal" class="_btn book">Book</a>
                    </div>
                </div>
            <?php
            }?>
        </div>
    </div>
</div>
<?php include('footer.php');?>
<!-- book modal  -->
<div class="modal fade themeModal bookModalBlock" id="bookModal" tabindex="-1" role="dialog" aria-labelledby="bookModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="col-12 modal-title text-center" id="bookModalTitle">Service</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body book-modal">
                <div class="card mt-2" id="req_saloon">
                    <div class="d-flex">
                        <div class="imgOuter1">
                            <img class="card-img-top rounded" src="assets/img/category/hair-setting.jpg" />
                        </div>
                        <div class="m-2 col_ryt">
                            <h5 >Service Provider Location</h5>
                            <p class="card-text">Some quick example text to build on the card title 
                                and make up the bulk of the card's content.</p>
                            <a href="#" id="bookformModal" data-toggle="modal" data-target="#bookModalForm" class="btnViewMore float-right" data-id="">BOOK AN APPOINTMENT</a>
                        </div>
                    </div>
                </div>
                <div class="card mt-2" id="req_home">
                    <div class="d-flex">
                        <div class="imgOuter1">
                            <img class="card-img-top rounded" src="assets/img/category/hair-setting.jpg" />
                        </div>
                        <div class="m-2 col_ryt">
                            <h5 >Service Provider Location </h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <a href="#" id="bookformModal1" data-id="" data-toggle="modal" data-target="#bookModalForm" class="btnViewMore float-right">REQUEST TO HOME</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- sample modal  -->
<div class="modal fade themeModal" id="bookModalForm" tabindex="-1" role="dialog" aria-labelledby="modalFormTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="col-12 modal-title text-center" id="modalFormTitle">Enter below details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="modalForm">
                    <div class="login">
                        <form method="POST" id="booking-form">
                            <input type="hidden" id="service_id">
                            <input type="hidden" id="service_provider_id">
                            <input type="hidden" id="type">
                            <div class="form-group mb-2">
                                <input type="text" id="sDate" class="form-control" autocomplete="off" required>
                                <label class="form-control-placeholder p-0" for="sDate">Date</label>
                            </div>
                            <div class="form-group mb-2">
                                <div class="boxedList">
                                    <div class="flxRow">
                                    <p id="slotLabel">Select Available Slot</p> 
                                    <i class="fa fa-caret-down" aria-hidden="true" id="showSlots"></i> 
                                </div>
                                    <div id="slotsList" style="display: none;">
                                    <ul id="slotData"></ul>
                                </div>
                                </div>
                            </div>
                            <!-- <div class="form-group mb-2">
                                <select name="service" class="form-control" autocomplete="off" required>
                                    <option hidden disabled selected value>
                                        Add List of Services </option>
                                    <option value="service1">service-1</option>
                                    <option value="service2">service-2</option>
                                    <option value="service3">service-3</option>
                                </select>
                            </div> -->

                            <div class="form-group mb-2">
                                <label>Check the Payment</label> 
                                <input type="text" id="price" name="price" value="Estimated Price: 00.00" class="form-control" autocomplete="off" required readonly/>
                                <input type="hidden" id="total_amount">
                            </div>
                            <div class="form-group mb-4">
                                <label>Preferences</label>
                                <div class="form-group mb-2">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        Male <input type="radio" class="form-check-input" name="optradio">
                                    </label>
                                  </div>
                                  <div class="form-check-inline">
                                    <label class="form-check-label">
                                        Female <input type="radio" class="form-check-input" name="optradio"> 
                                    </label>
                                  </div>
                                </div>
                            </div>
                            <div class="form-group mb-2">
                                <button type="button" id="btn-pay" class="btn theme-btn">Proceed To Pay</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://unpkg.com/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">

    $(".category_id").change(function() {
        //if(this.checked) 
        //{
            var sp_id = "<?php echo $sp_id;?>";

            var category_id = [];
            $("input:checkbox[name=category_id]:checked").each(function() {
                category_id.push($(this).val());
            });

            var formData = new FormData();

            formData.append('category_id', category_id);
            formData.append('sp_id', sp_id);

            $.ajax({
                type:'POST',
                url:'controllers/get_services.php',
                data:formData,
                contentType: false,
                processData: false,
                beforeSend: function(){
                    $(".mainDiv").html('<div class="img_container"><i class="fas fa-sync fa-spin"></i></div>');
                },
                success:function(res){

                    $(".cat-data").html(res);                          
                }
            });
        //}
    });

    $('#sDate').datepicker({
        format: 'yyyy-mm-dd',
        todayHighlight:true,
        autoclose:true,
        startDate:"<?php echo date("Y-m-d");?>",
        endDate :"<?php echo date("Y-m-d",strtotime("+20 days"));?>"

    });

    $('#bookformModal').click(function () {
        $('#bookModal').modal('hide');
        $('#type').val('salonee')
    })

    $('#bookformModal1').click(function () {
        $('#bookModal').modal('hide');
        $('#type').val('home')
    })

    $('#showSlots').click(function() {
        $('#slotsList').toggle('100');
        $(this).toggleClass("fa-caret-up fa-caret-down");
    });

    $('.book').click(function(){
        var service_id = $(this).closest("div.card").find("input[name='service_id']").val();
        var service_provider_id = $(this).closest("div.card").find("input[name='service_provider_id']").val();
        var service_at = $(this).closest("div.card").find("input[name='service_at']").val();

        if(service_at == 'home')
        {
            $('#req_saloon').hide();
            $('#req_home').show();
        }
        if(service_at == 'salonee')
        {
            $('#req_home').hide();
            $('#req_saloon').show();
        }
        if(service_at == 'both')
        {
            $('#req_saloon').show();
            $('#req_home').show();
        }

        $('#service_id').val(service_id)
        $('#service_provider_id').val(service_provider_id)
        
    });

    $("#sDate").change(function () { 

        var service_id = $("#service_id").val();
        var service_provider_id = $("#service_provider_id").val();
        var type = $("#type").val();
        var date = $(this).val();

        var formData = new FormData();

        formData.append('service_id', service_id);
        formData.append('service_provider_id', service_provider_id);
        formData.append('date', date);
        formData.append('type', type);

        $.ajax({
            type:'POST',
            url:'controllers/get_solts_by_service_id.php',
            data:formData,
            contentType: false,
            processData: false,
            success:function(res){

                $("#slotData").show();
                $('#slotsList').toggle('100');
                $("#slotData").html(res);                          
            }
        });

        var formData1 = new FormData();

        formData1.append('service_id', service_id);
        formData1.append('type', type);

        $.ajax({
            type:'POST',
            url:'controllers/get_service_price.php',
            data:formData1,
            contentType: false,
            processData: false,
            success:function(res){
                var res = JSON.parse(res);
                var slots = res.slots;
                var total_amount = slots[0].sale_price;
                if(slots.length > 0)
                {
                    $("#price").val('Estimated Price: '+total_amount+' AED');
                    $("#total_amount").val(total_amount);
                }
                else
                {
                    $("#price").val('Estimated Price: 00:00');
                }                              
            }
        });
    });

    $("#btn-pay").click(function(){

        var user_id = "<?php echo @$_SESSION['user_id'];?>";

        if(user_id == '')
        {
            $('#bookModalForm').modal('hide');
            $('#loginModal').modal('show');
        }
        else
        {
            var slot_time = $('input:radio[name=slot_time]:checked').val();
            var service_id = $("#service_id").val();
            var service_provider_id = $("#service_provider_id").val();
            var type = $("#type").val();
            var slot_date = $("#sDate").val();

            var url = 'payment/payment.php';
            var form = $('<form action="' + url + '" method="post">' +
              '<input type="text" name="user_id" value="' + user_id + '" />' +
              '<input type="text" name="slot_date" value="' + slot_date + '" />' +
              '<input type="text" name="slot_time" value="' + slot_time + '" />' +
              '<input type="text" name="service_id" value="' + service_id + '" />' +
              '<input type="text" name="service_provider_id" value="' + service_provider_id + '" />' +
              '<input type="text" name="type" value="' + type + '" />' +
              '</form>');
            $('body').append(form);
            form.submit();
        }
    });
</script>