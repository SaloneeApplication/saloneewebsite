<?php 
include('header.php');
$page = 'profile';

$user_id = @$_SESSION['user_id'];

if($user_id == "")
{
    echo '<script> var base_url = "http://localhost/salonee_web/"; </script>';
    echo '<script> window.location.replace(base_url); </script>';
}

$userDetails = $funcObject->getuserDetails($con, $user_id);
$row = mysqli_fetch_array($userDetails);

?>
<div class="container-fluid">
<div class="_header"></div>
<!-- breadcrumb  -->
<nav aria-label="breadcrumb" class="_custmBrdcrmb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#"><?php echo $nav_link_data[0]['my_account'];?></a></li>
        <li class="breadcrumb-item active" aria-current="page"><?php echo $nav_link_data[0]['profile'];?></li>
    </ol>
</nav>
<div class="d-flex myFlex">
    <?php include('sidebar.php');?>
    <div class="mainDiv _bgWyt">
        <div id="profileInfo">
            <div class="profile_details">
                <div class="picture-container">
                        <div class="picture">
                        <img src="uploads/<?php echo $row['image'];?>" alt="avatar" class="picture-src" id="wizardPicturePreview">
                    </div>
                </div>
                <ul>
                    <li>
                        <span>Name</span>
                        <p><?php echo $row['name'];?></p>
                    </li>
                    <li>
                        <span>Email Id</span>
                        <p><?php echo $row['email'];?></p>
                    </li>
                    <li>
                        <span>Mobile Number</span>
                        <p><?php echo $row['mobile'];?></p>
                    </li>
                    <li>
                        <span>Area</span>
                        <p><?php echo $row['area'];?></p>
                    </li>
                </ul>
            </div>
            <div class="editBtnBlock">
                <form>
                    <div class="form-group">
                        <button type="button" class="btn theme-btn" id="edit_profile">Edit Profile</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- edit profile fields -->
        <div id="editPrfile" style="display: none;">
            <div>
                <form id="editProfileForm">
                    <div class="picture-container">
                        <div class="picture">
                            <img src="uploads/<?php echo $row['image'];?>" class="picture-src" id="wizardPicturePreview" title="">
                            <input type="file" id="wizard-picture" class="" name="image">
                            <i class="fa fa-camera" aria-hidden="true"></i>
                        </div>
                        <!-- <h6 class="">Choose Picture</h6> -->
                    </div>
                    <div class="login wdthLmt">                    
                        <input type="hidden" name="user_id" id="user_id" value="<?php echo $row['user_id'];?>">
                        <div class="form-group mt-4 ">
                            <input type="text" id="name" class="form-control" name="name" value="<?php echo $row['name'];?>" autocomplete="off" required>
                            <label class="form-control-placeholder p-0" for="name">Name</label>
                        </div>
                        <div class="form-group">
                            <input type="text" id="email" class="form-control" name="email" value="<?php echo $row['email'];?>" autocomplete="off" required>
                            <label class="form-control-placeholder p-0" for="email">Email Id</label>
                        </div>
                        <div class="form-group">
                            <input type="text" id="mobile" class="form-control" name="mobile" value="<?php echo $row['mobile'];?>" autocomplete="off" required>
                            <label class="form-control-placeholder p-0" for="mn">Mobile Number</label>
                        </div>
                        <div class="form-group">
                            <input type="text" id="location" class="form-control" name="location" value="<?php echo $row['area'];?>" autocomplete="off" required>
                            <label class="form-control-placeholder p-0" for="location">Area</label>
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn theme-btn" id="saveBtn">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--end Main Div-->
</div>
<?php include('footer.php');?>
<script>

    $('#edit_profile').click(function(){
        $('#editPrfile').show();
        $('#profileInfo').hide();
    });

    // SIGNUP FORM SUBMIT
    $("#saveBtn").click(function () {  

        var user_id = $("#user_id").val();
        var name = $("#name").val();
        var email = $("#email").val();
        var mobile = $("#mobile").val();
        var area = $("#location").val();
        var image = $('#wizard-picture')[0].files[0];

        var formData = new FormData();

        formData.append('user_id', user_id);
        formData.append('name', name);
        formData.append('email', email);
        formData.append('mobile', mobile);
        formData.append('area', area);
        formData.append('image', image);

        $.ajax({
            type:'POST',
            url:'controllers/edit_profile.php',
            data:formData,
            contentType: false,
            processData: false,
            success:function(html){
                if(html == 1)
                {
                    swal({
                        type: "success",
                        text: 'Profile updated Successfully',
                        showConfirmButton: false,
                        timer: 1500
                    });
                    setInterval('location.reload()', 1500); 
                }
                else
                {
                    swal({
                        type: "error",
                        text: 'Something went wrong',
                        showConfirmButton: true,
                        timer: 1500
                    });
                    //setInterval('location.reload()', 1500);
                }                               
            }
        });
    });

</script>