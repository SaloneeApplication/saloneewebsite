<?php
include('header.php'); 
$page = 'refer'; 

$user_id = @$_SESSION['user_id'];

if($user_id == "")
{
    echo '<script> var base_url = "http://localhost/salonee_web/"; </script>';
    echo '<script> window.location.replace(base_url); </script>';
}

 ?>

<div class="container-fluid">
  <div class="_header"></div>
  <!-- breadcrumb  -->
  <nav aria-label="breadcrumb" class="_custmBrdcrmb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item"><a href="#">My Account</a></li>
      <li class="breadcrumb-item active" aria-current="page">Refer &amp; Earn</li>
    </ol>
  </nav>

<div class="d-flex myFlex">
  <?php include('sidebar.php');?>

  <div class="mainDiv _bgWyt">

    
    <div class="refer_N_earn">
       <img src="./assets/img/refer-and-earn.jpg" alt="refer-and-earn" class="refrImgBg">

       <div class="refr_content">
          <h5>Your Referal Code</h5>
          <div class="shareUrl-body">
            <div class="container">
              <!-- COPY INPUT -->
              <input class="shareUrl-input js-shareUrl" type="text" readonly="readonly" /> 
            </div>
          </div> 
          <h5>Invite Your Friends</h5>
          <a href="#"><img src="assets/img/icons/fb.svg" alt="facebook" /> </a>
          <a href="#"><img src="assets/img/icons/g-plus.svg" alt="g-plus" /></a>
       </div>

       <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. 
         Quis sunt dignissimos fugiat porro ad quod autem placeat a quia temporibus labore eum 
         tempora magni commodi mollitia adipisci odio, doloremque sint officia aliquam animi! Ratione, 
         inventore architecto quia et molestias in!</p>
    </div>

</div> <!--end Main Div-->


</div>
<?php include('footer.php');?>
