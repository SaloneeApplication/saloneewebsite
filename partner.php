<?php include('header.php');?>
        <!-- page title -->
        <div class="ttm-page-title-row">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="title-box text-center">
                            <div class="page-title-heading">
                                <h1>PARTNER</h1>
                            </div>
                            <!-- /.page-title-captions -->
                            <div class="breadcrumb-wrapper"> <span><a title="Homepage" href="index-english.php">home</a></span>
                                <span class="ttm-bread-sep">&nbsp; / &nbsp;</span>
                                <span>Partner</span>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-md-12 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </div>
        <!-- page title -->
        

         <section class="ttm-row bg-img12 contact-section res-767-p-15 clearfix">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-9 col-md-9 col-sm-9 m-auto text-center pt-90 res-991-pt-40 res-767-pt-0">
                             <div class="title-header revealOnScroll pl-25" data-animation="fadeInLeft" data-timeout="200"">
                                    <h2 class="text-start color-pink font-weight-bold">Boost your Sales. <br>
                                        Explore a Great Customer Network.<br/>
                                        Join Now.
                                    </h2>
                                    <p class="text-start ">Salonee invites you to join UAE’s largest platform with a gigantic network <br>of Salon & Spa under one umbrella.</p>
                                </div>
                            <div class="section-title with-desc clearfix">
                               

                                <div class="card-group boost-sales">
                                    <div class="col-lg-3 col-md-6 p-0">
                                        <div class="card bg-transparent p-2 border-0 revealOnScroll h-100" data-animation="fadeInLeft" data-timeout="200"">
                                            <img class="" src="assets/img/img-1.png" alt="" width="105" height="55" style="margin: 15px;">
                                            <div class="card-body text-start border-right">
                                            <h5 class="card-title p-1">Delightful User Experience</h5>
                                            <p class="card-text p-1">Best user interface from Booking Services to Billing, an online application accessible on multiple platforms - PC, Laptop, Tablets & Smart Phones. </p> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 p-0">
                                <div class="card bg-transparent p-2 border-0 revealOnScroll h-100" data-animation="fadeInLeft" data-timeout="200"">
                                    <img class="" src="assets/img/img-2.png" alt="" width="53" height="53" style="margin: 15px;">
                                    <div class="card-body text-start border-right border-cstm">
                                      <h5 class="card-title p-1">Artificial Intelligence</h5>
                                      <p class="card-text p-1">Suggestive selling as per the customer's behaviour & their style choices.</p> 
                                    </div>
                                </div>
                                </div>
                                <div class="col-lg-3 col-md-6 p-0">
                                <div class="card bg-transparent p-2 border-0 revealOnScroll h-100" data-animation="fadeInLeft" data-timeout="200"">
                                    <img class="" src="assets/img/img-3.png" alt="" width="72" height="28" style="margin: 15px;">
                                    <div class="card-body text-start border-right">
                                      <h5 class="card-title p-1">Business Growth With Apps</h5>
                                      <p class="card-text p-1">This is a wider card with supporting text below as a natural lead-in to additional content. This card has even longer content than the first to show that equal height action.</p> 
                                    </div>
                                </div>
                                </div>
                                <div class="col-lg-3 col-md-6 p-0">
                                <div class="card bg-transparent p-2 border-0 revealOnScroll h-100" data-animation="fadeInLeft" data-timeout="200"">
                                    <img class="" src="assets/img/img-4.png" alt="" width="63" height="46" style="margin: 15px;">
                                    <div class="card-body text-start">
                                      <h5 class="card-title p-1">Customer Delight</h5>
                                      <p class="card-text p-1">Memberships, Wallets, Loyalty programs, Packages to get you the best of opportunities to stay in business.</p> 
                                    </div>
                                </div>
                                </div>

                            </div>


                            </div>
                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-1 d-none d-lg-block"></div>
                    </div>
                </div>
            </section>

            <section class="ttm-row bg-img3 contact-section res-767-p-15 clearfix" style="background-color: #d95f98;">
                <div class="container-fluid">
                <div class="row">

                        <div class="container">
                        <div class="row justify-content-md-center m-3">
                        <div class="col-md-4 text-center">                        
                                  <div class="circle m-mid" style="width: 280px;">
                                      <div class="circle__inner">
                                        <div class="circle__wrapper">
                                          <div class="circle__content">
                                                <h2 class="ttm-textcolor-white">Join us today <br><span class="" style="font-size: 16px;">REGISTER NOW</span></h2>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                        </div>

                        <div class="col-lg-7">
                        <form action="" class="login1 mt-30">
                            <div class="form-group">
                                <input name="name" type="text" class="form-control" placeholder="Your Name*" required="required">
                            </div>
                            <div class="form-group">
                                <input name="name" type="text" class="form-control" placeholder="BUSINESS NAME" required="required">
                            </div>
                            <div class="form-group">
                                <input name="name" type="text" class="form-control" placeholder="+971  |  5362878960" required="required">
                            </div>
                            <div class="form-group">
                               <div class="form-check">
                                  <input class="p-2" type="checkbox" value="" id="flexCheckDefault">
                                  <label class="form-check-label" for="flexCheckDefault">
                                    I accept Terms & Conditions
                                  </label>
                                 </div> 
                            </div>

                           <div class="form-group">
                                <button type="submit" id="submit" class="ttm-btn ttm-btn-size-sm ttm-textcolor-white ttm-btn-shape-round" value="">SIGN UP</button>
                            </div>

                        </form>
                        </div>
                         
                        </div>

                        </div>

                        </div>
                    </div>
            </section>  

            <section class="ttm-row bg-img6 contact-section res-767-p-15 clearfix">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-2 d-none d-lg-block"></div>
                        <div class="col-lg-9 col-md-9 col-sm-9 m-auto pt-90 res-991-pt-40 res-767-pt-0">

                            <div class="section-title with-desc clearfix">
                                <div class="title-header revealOnScroll" data-animation="fadeInRight" data-timeout="200"">
                                    <h2 class="text-start color-pink">HOW THE <strong>APP WORKS?</strong></h2>                                    
                                </div>
                                <!-- <div class="row">
                                    <div class="col-md-12" align="left"><img class="" src="assets/img/img-all.png" alt="" width="573" height="53" style="margin: 15px;"></div>
                                </div> -->

                                <div class="row" id="feature-main">
                                    <div class="col-xl-2 col-lg-4 col-md-6 revealOnScroll" data-animation="fadeInLeft" data-timeout="200"">
                                        <div class="featur-icon">
                                            <span class="featur-img">
                                                <img src="assets/img/ico-1.png" alt="">
                                            </span>
                                        </div>
                                        <div class="featur-txt">
                                            <h6 class="">Book a Salon</h6>
                                            <p class="">Get direct booking from Salonee App & start adding up customers onto your list of new clienteles once you sign up with Salonee.</p>
                                        </div>
                                    </div>
                                    <div class="col-xl-2 col-lg-4 col-md-6 revealOnScroll" data-animation="fadeInRight" data-timeout="200"">
                                        <div class="featur-icon">
                                            <span class="featur-img">
                                                <img src="assets/img/ico-2.png" alt="">
                                            </span>
                                        </div>
                                        <div class="featur-txt">
                                            <h6 class="">Admit Customer</h6>
                                            <p class="">You can start taking bookings / appointments from anywhere & at any time of the day, with just a click of a button.</p>
                                        </div>
                                    </div>
                                    <div class="col-xl-2 col-lg-4 col-md-6 revealOnScroll" data-animation="fadeInLeft" data-timeout="200"">
                                        <div class="featur-icon">
                                            <span class="featur-img">
                                                <img src="assets/img/ico-3.png" alt="">
                                            </span>
                                        </div>
                                        <div class="featur-txt">
                                            <h6 class="">Schedule Bookings</h6>
                                            <p class="">Welcome to a world of resource management to find perfect work-life balance. Salonee helps you <p id="readmore1" class="collapse">schedule / reschedule
                                            / cancel appointments & bookings with ease & manage them seamlessly.
                                            </p><a href="#readmore1" data-toggle="collapse" class="color-pink readbtn"> >>Read More</a><a href="#readmore1" data-toggle="collapse" class="color-pink readless hide"> >>Read Less</a> </p>
                                        </div>
                                    </div>
                                    <div class="col-xl-2 col-lg-4 col-md-6 revealOnScroll" data-animation="fadeInRight" data-timeout="200"">
                                        <div class="featur-icon">
                                            <span class="featur-img">
                                                <img src="assets/img/ico-4.png" alt="">
                                            </span>
                                        </div>
                                        <div class="featur-txt">
                                            <h6 class="">Customer Retention</h6>
                                            <p class="">Connect with your clients, stay in touch, provide excellent customer service, get good reviews<p id="readmore2" class="collapse"> & eventually watch your business grow.</p><a href="#readmore2" data-toggle="collapse" class="color-pink readbtn2"> >>Read More</a><a href="#readmore2" data-toggle="collapse" class="color-pink readless2 hide"> >>Read Less</a></p>
                                        </div>
                                    </div>
                                    <div class="col-xl-2 col-lg-4 col-md-6 revealOnScroll" data-animation="fadeInLeft" data-timeout="200"">
                                        <div class="featur-icon">
                                            <span class="featur-img">
                                                <img src="assets/img/ico-5.png" alt="">
                                            </span>
                                        </div>
                                        <div class="featur-txt">
                                            <h6 class="">Electronic Payment System</h6>
                                            <p class="">Customers can make payments online & go cashless with an absolute secured payment <p id="readmore3" class="collapse">system, through Salonee App. A hassle-free & an easy process to pay for the services availed.</p><a href="#readmore3" data-toggle="collapse" class="color-pink readbtn3"> >>Read More</a><a href="#readmore3" data-toggle="collapse" class="color-pink readless3 hide"> >>Read Less</a></p>
                                        </div>
                                    </div>
                                    <div class="col-xl-2 col-lg-4 col-md-6 revealOnScroll" data-animation="fadeInRight" data-timeout="200"">
                                        <div class="featur-icon last-ico">
                                            <span class="featur-img">
                                                <img src="assets/img/ico-6.png" alt="">
                                            </span>
                                        </div>
                                        <div class="featur-txt">
                                            <h6 class="">Salon at home</h6>
                                            <p class="">This service is one of the highlights for Customers who want to avail special salon services for differently abled <p id="readmore4" class="collapse">children & senior citizens. Basic care services like hair & nail cutting done by trained professionals at doorstep. Salons interested in making this as a part of their offerings can step in to list it as one of their services.</p><a href="#readmore4" data-toggle="collapse" class="color-pink readbtn4"> >>Read More</a><a href="#readmore4" data-toggle="collapse" class="color-pink readless4 hide"> >>Read Less</a></p>
                                        </div>
                                    </div>
                                    
                                     
                                    
                                       
                                </div>
 

                            </div>
                        </div>
                        
                    </div>
                </div>
            </section>

            <section class="ttm-row contact-section res-767-p-15 clearfix">
                <div class="container pt-90 p-3 app-features">
                    <div class="row">
                        <div class="title-header">
                                        <!-- <h5 class="ttm-textcolor-skincolor text-center revealOnScroll" data-animation="fadeInRight" data-timeout="400">Payments</h5> -->
                                        <h2 class="title2 text-start revealOnScroll color-pink" data-animation="fadeInLeft" data-timeout="200"><strong>POWER-PACKED</strong> APPLICATION FEATURES</h2> 
                                    </div>
                        <div class="col-md-6 res-767-pt-40">
                            <div class="spacing-8">
                                <div class="section-title with-desc clearfix">                                    
                                    <div class="title-desc revealOnScroll" data-animation="fadeInRight" data-timeout="400"><img class="feat-icon" src="assets/img/img-11.png" alt=""><br><br>Salonee App is designed to deliver & boost your business whether it is a Salon or a Spa, any beauty service, Salonee gives you all the great tools you need to run your business with ease & precision.</div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 res-767-center">
                            <img src="assets/img/web-banner.png" class="img-fluid revealOnScroll" data-animation="flipInY" data-timeout="400" alt="bg-image">
                        </div>
                        
                        <div class="col-lg-3 col-md-6 res-767-pt-40">
                            <div class="spacing-8">
                                <div class="section-title with-desc clearfix">
                                     
                                    <div class="title-desc revealOnScroll application-features" data-animation="fadeInRight" data-timeout="400">
                                        <img class="feat-icon" src="assets/img/img-12.png" alt="">
                                        <h5>Centralized Database</h5>
                                        <p>Single Database for all locations & Salons. Centrally located database for easy access, resulting in enhanced degrees of control & coordination.</p></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6 res-767-pt-40">
                            <div class="spacing-8">
                                <div class="section-title with-desc clearfix">
                                    
                                    <div class="title-desc revealOnScroll application-features" data-animation="fadeInRight" data-timeout="400">
                                        <img class="feat-icon" src="assets/img/img-13.png" alt="">
                                    <h5>Customer Listing</h5>
                                    <p>Customer records are stored centrally in the system and shared with all your Salons, for customer to get a seamless experience.</p><p id="readmore5" class="collapse"> Everything about their last visits, frequency of visits, their preferences, and whole profile.</p><a href="#readmore5" data-toggle="collapse" class="color-pink readbtn5"> >>Read More</a><a href="#readmore5" data-toggle="collapse" class="color-pink readless5 hide"> >>Read Less</a></div>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="col-md-4 res-767-pt-40">
                            <div class="spacing-8">
                                <div class="section-title with-desc clearfix">
                                     
                                    <div class="title-desc revealOnScroll application-features" data-animation="fadeInRight" data-timeout="400">
                                        <img src="assets/img/img-14.png"  alt="">
                                    <h5>Service Menu</h5>
                                    <p>Salonee App is designed to deliver & boost your business whether it is a Salon or a Spa, any beauty service, Salonee gives you all the great tools you need to run your business with ease & precision.</p>
                                </div>
                                </div>
                            </div>
                        </div> -->

                        <div class="col-lg-3 col-md-6 res-767-pt-40">
                            <div class="spacing-8">
                                <div class="section-title with-desc clearfix">
                                     
                                    <div class="title-desc revealOnScroll application-features" data-animation="fadeInRight" data-timeout="400">
                                        <img class="feat-icon" src="assets/img/img-15.png"  alt="">
                                        <h5>Manage Campaign</h5>
                                        <p>Run Campaigns throughout the network of Salons, coupon codes, discounts which can be applied centrally. Issuance of</p> <p id="readmore6" class="collapse">coupons, discounted packages, wallets, online payment and much more.</p><a href="#readmore6" data-toggle="collapse" class="color-pink readbtn6"> >>Read More</a><a href="#readmore6" data-toggle="collapse" class="color-pink readless6 hide"> >>Read Less</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6 res-767-pt-40">
                            <div class="spacing-8">
                                <div class="section-title with-desc clearfix">
                                     
                                    <div class="title-desc revealOnScroll application-features" data-animation="fadeInRight" data-timeout="400">
                                        <img class="feat-icon" src="assets/img/img-16.png"  alt="">
                                    <h5>Manage Globally</h5>
                                    <p>Multiple Salon business can be managed from the comfort of your home, or while you are on the road. A system designed to help</p> <p id="readmore7" class="collapse">you monitor, manage and make your business accessible from anywhere, globally. Easy to use interface to Analyse, Understand and Control.</p><a href="#readmore7" data-toggle="collapse" class="color-pink readbtn7"> >>Read More</a><a href="#readmore7" data-toggle="collapse" class="color-pink readless7 hide"> >>Read Less</a></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>

 <section class="ttm-row bg-img3 contact-section res-767-p-15 clearfix bg-pink">
                <div class="container-fluid">
                <div class="row">

                        <div class="container">
                        <div class="row justify-content-md-center mt-50 m-b-0">
                        
                        <div class="col-lg-4 col-md-6 text-center">
                        <img src="assets/img/web-mobile.png" alt="" class="align-middle revealOnScroll" data-animation="flipInY" data-timeout="400"> </div>

                        <div class=" col-lg-8 col-md-6">
                            <h2 class="ttm-textcolor-white demo-text" style="font-size:40px; font-weight:800;">Free Demo </h2>
                        <form action="" class="login1">
                            <div class="form-group">
                                <input name="name" type="text" class="form-control" placeholder="Your Name*" required="required">
                            </div>
                            <div class="form-group">
                                <input name="name" type="text" class="form-control" placeholder="Email" required="required">
                            </div>
                            <div class="form-group">
                                <input name="name" type="text" class="form-control" placeholder="Phone" required="required">
                            </div>
                            
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Message" style="border-radius: 20px; height: 80px; padding: 20px;"></textarea>

                            </div> 
                            <div class="form-group" align="center">
                                <button type="submit" id="submit" class="ttm-btn ttm-btn-size-sm ttm-textcolor-white ttm-btn-shape-round" value="">
                                    SUBMIT
                                </button>
                            </div>

                        </form>
                        </div>
                         
                        </div>

                        </div>

                        </div>
                    </div>
            </section>  

            <section class="ttm-row bg-img11 contact-section res-767-p-15 clearfix">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-9 col-md-9 col-sm-9 m-auto text-center pt-90 res-991-pt-40 res-767-pt-0">
                            <div class="section-title with-desc clearfix">
                                <div class="title-header">
                                    <h2 class="color-pink">BENEFITS OF <strong>LISTING YOUR SALONS</strong></h2>                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-12" align="left">
                                        <p class="card-text p-t-2 pb-4">This is an administrative dashboard that will allow you to stay updated with the current status & performance of your salon.
You can track, monitor & assess the sales, daily appointment schedule, customer details, invoices. You will never miss a data again!</p> 

                                    </div>
                                </div>

                             <div class="card-deck g-0 benefit-cards">
                                <div class="col-lg-3 col-md-6 p-0">
                                  <div class="card h-100 border-3 shadow bg-white rounded revealOnScroll" data-animation="fadeInRight" data-timeout="400">
                                    <div class="card-body card-border">
                                       <img class="" src="assets/img/img-17.png" alt="" style="margin: 15px;">
                                       <h5>Analytics</h5>
                                      <p class="card-text p-1">Tracking popular services, customer schedules & booking history.</p> 
                                    </div>
                                  </div>
                                </div>
                                <div class="col-lg-3 col-md-6 p-0">
                                   <div class="card h-100 border-3 shadow bg-white rounded revealOnScroll" data-animation="fadeInLeft" data-timeout="400">
                                    <div class="card-body card-border">
                                       <img class="" src="assets/img/img-18.png" alt="" style="margin: 15px;">
                                       <h5>Customers List</h5>
                                      <p class="card-text p-1">Increase your opportunity to gain customers and boost your business.</p> 
                                    </div>
                                  </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6 p-0 top-space">
                                  <div class="card h-100 border-3 shadow bg-white rounded revealOnScroll" data-animation="fadeInRight" data-timeout="400">
                                    <div class="card-body card-border">
                                       <img class="" src="assets/img/img-19.png" alt="" style="margin: 15px;">
                                       <h5>Market Trends</h5>
                                      <p class="card-text p-1">Identifying & forecasting Market Trends for planning business in the long run.</p> 
                                    </div>
                                  </div>
                                                </div>
                                                <div class="col-lg-3 col-md-6 p-0 top-space">
                                  <div class="card h-100 border-3 shadow bg-white rounded revealOnScroll" data-animation="fadeInLeft" data-timeout="400">
                                    <div class="card-body card-border">
                                       <img class="" src="assets/img/img-20.png" alt="" style="margin: 15px;">
                                       <h5>Exhibit Skills</h5>
                                      <p class="card-text p-1">Increase your opportunity to showcase your talent, get reviews or ratings & become a top-rated salon.</p> 
                                    </div>
                                  </div>
                                                </div>

                                </div>


                            </div>
                        </div>
                        
                    </div>
                </div>
            </section>



        <section class="ttm-row bg-img3 contact-section res-767-p-15 clearfix bg-gray">
                
                <div class=" pb-70 res-991-pb-20">
                    <div class="container">
                        <div class="row">
                             <div class="col-12 mt-100 mb-50">
                                 <img class="mx-auto d-block revealOnScroll img-cstm"  src="assets/img/mobile-bg.png" alt="" data-animation="flipInY" data-timeout="400">
                            </div>
                            
                        </div>
                    </div>
                </div>
            </section>


 <!-- testimonials -->            
            <section class="ttm-row testimonial-section  p-0 clearfix">
                <div class="container p-5">
                    <!-- service slider -->
                    <div class="row">
                        <div class="col-12 p-0">
                             <div class="title-header">
                                    <h2 class="text-start" style="color: #e83e8c; text-align: left;"><strong>TESTIMONIALS</strong></h2>                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-12" align="left">
                                        <h4>Our Happy Clients</h4>
                                        <p class="card-text p-t-2 pb-4">An Application that helps you reach out to customers from across UAE, get more visibility & it includes fantastic features such booking appointments, booking history, reports / analytics system, & best part is you can list for free.
Also, sign up for a minimum annual charge to get more amazing benefits that were possible only in your dreams.</p> 

                                    </div>
                                </div>
                        </div>
                        
                    </div>
                    <!-- service slider end-->
                    <!-- testimonials -->
                    <div class="row">
                        <div class="col-lg-10 col-md-12 m-auto">
                            <div class="testimonial-slide style1 owl-carousel pt-60 res-991-pt-40" data-item="1" data-nav="true" data-dots="true" data-auto="true">
                                <div class="testimonials style1 text-center"> 
                                    <div class="testimonial-content">
                                        <div class="testimonial-avatar">
                                            <div class="testimonial-img">
                                                <img class="img-center img-rounded" src="assets/img/testimonial/1.jpg" alt="testimonial-img">
                                            </div>
                                        </div>
                                        <h3>Wonderful App! This has sorted my salon & my life. </h3>
                                        <p>Helps me track customers & their booking history. Helps me forecast & plan my future expenses & presents a clear picture of my ROI.</p>
                                        <div class="ttm-ratting-star">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <div class="testimonial-caption">
                                            <h6>Asef Mohammed</h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="testimonials style1 text-center"> 
                                    <div class="testimonial-content">
                                        <div class="testimonial-avatar">
                                            <div class="testimonial-img">
                                                <img class="img-center img-rounded" src="assets/img/testimonial/02.jpg" alt="testimonial-img">
                                            </div>
                                        </div>
                                        <h3>Best Salon App!</h3>
                                        <p>I have been waiting for something like this that can help me manage my salon while I manage my home. Keeping in touch with Customers, scheduling slots for bookings was never this easy. Salonee is a Savior!!! </p>
                                        <div class="ttm-ratting-star">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <div class="testimonial-caption">
                                            <h6>Laila Ahmed</h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="testimonials style1 text-center"> 
                                    <div class="testimonial-content">
                                        <div class="testimonial-avatar">
                                            <div class="testimonial-img">
                                                <img class="img-center img-rounded" src="assets/img/testimonial/03.jpg" alt="testimonial-img">
                                            </div>
                                        </div>
                                        <h3>Absolute Delight</h3>
                                        <p>Salonee is! I don’t need to hire an ops manager to manage my 3 salon branches, I can do it all with just one app and on my phone. I am loving it.</p>
                                        <div class="ttm-ratting-star">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <div class="testimonial-caption">
                                            <h6>Rihana Rahman</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                    <!-- testimonials end -->
                </div>

                
            </section>
             <!-- testimonials end -->

<?php include('footer.php');?>
    <script>
    $(".readbtn").click(function(){$(this).addClass("hide")}),$(".readbtn").click(function(){$(".readless").removeClass("hide")}),$(".readless").click(function(){$(this).addClass("hide")}),$(".readless").click(function(){$(".readbtn").removeClass("hide")}),$(".readbtn2").click(function(){$(this).addClass("hide")}),$(".readbtn2").click(function(){$(".readless2").removeClass("hide")}),$(".readless2").click(function(){$(this).addClass("hide")}),$(".readless2").click(function(){$(".readbtn2").removeClass("hide")}),$(".readbtn3").click(function(){$(this).addClass("hide")}),$(".readbtn3").click(function(){$(".readless3").removeClass("hide")}),$(".readless3").click(function(){$(this).addClass("hide")}),$(".readless3").click(function(){$(".readbtn3").removeClass("hide")}),$(".readbtn4").click(function(){$(this).addClass("hide")}),$(".readbtn4").click(function(){$(".readless4").removeClass("hide")}),$(".readless4").click(function(){$(this).addClass("hide")}),$(".readless4").click(function(){$(".readbtn4").removeClass("hide")});
    $(".readbtn5").click(function(){$(this).addClass("hide")}),$(".readbtn5").click(function(){$(".readless5").removeClass("hide")}),$(".readless5").click(function(){$(this).addClass("hide")}),$(".readless5").click(function(){$(".readbtn5").removeClass("hide")});
    $(".readbtn6").click(function(){$(this).addClass("hide")}),$(".readbtn6").click(function(){$(".readless6").removeClass("hide")}),$(".readless6").click(function(){$(this).addClass("hide")}),$(".readless6").click(function(){$(".readbtn6").removeClass("hide")});
    $(".readbtn7").click(function(){$(this).addClass("hide")}),$(".readbtn7").click(function(){$(".readless7").removeClass("hide")}),$(".readless7").click(function(){$(this).addClass("hide")}),$(".readless7").click(function(){$(".readbtn7").removeClass("hide")});
    </script>