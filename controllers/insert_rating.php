<?php 

	include("../config/dbConnection.php");
	include("functions.php");

	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
	//Retrieving Form Fields
	$service_provider_id = $_POST['service_provider_id'];
	$user_id = $_POST['user_id'];
	$service_id = $_POST['service_id'];
	$rating = $_POST['rating'];
	$description = $_POST['description'];
	$created_time = date('Y-m-d H:i:s');

	if($con)
    {
    	$sql1 = "SELECT id FROM ratings WHERE user_id = '$user_id' AND service_id = '$service_id'";
    	$recordSet1 = mysqli_query($con,$sql1);
    	$ratingCount = mysqli_num_rows($recordSet1);

    	while($row1 = mysqli_fetch_array($recordSet1))
		{
			$rating_id = $row1['id'];
		}

    	if($ratingCount > 0)
    	{
    		$sql = "UPDATE ratings SET rating = '$rating', comment = '$description', modified_time = '$created_time' 
    		WHERE id = '$rating_id' ";

	        $rowsAffected = mysqli_query($con,$sql); 

	        if($rowsAffected > 0)
	        {
	        	echo 3;
	        } 
	        else
	        {
	        	echo 2;
	        }
    	}
    	else
    	{
	        $sql = "INSERT INTO ratings (service_provider_id, user_id, service_id, rating, comment, created_time) 
	        		VALUES ('$service_provider_id', '$user_id', '$service_id', '$rating', '$description', '$created_time')";

	        $rowsAffected = mysqli_query($con,$sql);

	        if($rowsAffected > 0)
	        {
	        	echo 1;
	        } 
	        else
	        {
	        	echo 2;
	        }
	    }
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	} 
	
?>