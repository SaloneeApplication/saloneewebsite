<?php 

	include("../config/dbConnection.php");
	include("functions.php");

	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
	//Retrieving Form Fields
	$name = $_POST['full_name'];
	$email = $_POST['email'];
	$password = $_POST['password'];
	$mobile_code = $_POST['mobile_code'];
	$mobile = $_POST['mobile_number'];
	$gender = $_POST['gender'];
	$area = $_POST['area'];

	function generateNumericOTP($n) 
	{
		$generator = "1357902468";
	    $result = "";
	  
	    for ($i = 1; $i <= $n; $i++) {
	        $result .= substr($generator, (rand()%(strlen($generator))), 1);
	    }
	  
	    // Return result
	    return $result;
	}

	$otp = generateNumericOTP(4);
	$message = "Use $otp as your verification code on Salonee. Do not share your OTP with anyone.";

	$url = "https://smsapi.24x7sms.com/api_2.0/SendSMS.aspx?APIKEY=ee7dzmQudDi&MobileNo=$mobile_code$mobile&SenderID=TESTID&Message=$message&ServiceName=INTERNATIONAL";
    $ch = curl_init(); 
    curl_setopt($ch, CURLOPT_URL, $url); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    $output = curl_exec($ch);
    curl_close($ch);

	if($con)
    {
        $sql = "INSERT INTO user (name, email, password, mobile_code, mobile, otp, gender, area) VALUES ('".$name."', '".$email."', '".$password."', '".$mobile_code."', '".$mobile."', '".$otp."', '".$gender."', '".$area."')";

        $rowsAffected = mysqli_query($con,$sql);
        $user_id = $con->insert_id;

        if($rowsAffected > 0)
        {	
        	echo $user_id;
        } 
        else
        {
        	echo FALSE;
        }
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	} 
	
?>