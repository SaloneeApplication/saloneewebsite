<?php 

	include("../config/dbConnection.php");

	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
    $slot_id = $_POST['slot_id'];

    $sql = "UPDATE service_slots SET service_status='cancelled'
            WHERE slot_id='$slot_id'";

    $rowsAffected = mysqli_query($con,$sql);

    if($rowsAffected > 0)
    {
        $result['status'] = 200;
        $result['message'] = 'Booking cancelled successfully';
    }
    else
    {
        $result['status'] = 400;
        $result['message'] = 'Something went wrong';
    }    

    echo json_encode($result);
?>