<?php
include("../config/dbConnection.php");
include("functions.php");

$dbObject = new dbConnection();
$con = $dbObject->getConnection();

$funcObject = new functions();

$category_id = @$_POST['category_id'];
$sp_id = @$_POST['sp_id'];

$category_id = str_replace(',', '|', $category_id);

$sql = "SELECT ss.service_provider_service_id as service_id, ss.service_provider_id, ss.service_name as name, 
		            ss.service_at, ss.description, sp.image, sp.business_name as service_provider_name
		            FROM service_provider_services ss 
		            JOIN service_provider sp ON sp.service_provider_id = ss.service_provider_id";

        if($category_id != "" && $sp_id == "")
        {
        	$sql .= " WHERE CONCAT(',', ss.category_id, ',') REGEXP ',(".$category_id."),' ";
        }
        else if($category_id != "" && $sp_id != "")
        {
        	$sql .= " WHERE ss.service_provider_id = '$sp_id' AND CONCAT(',', ss.category_id, ',') REGEXP ',(".$category_id."),' ";
        }
        else
        {
        	$sql .= " WHERE ss.service_provider_id = '$sp_id'";
        }
        
        $services = mysqli_query($con,$sql);

		while($row = mysqli_fetch_array($services))
		{
			echo   '<div class="card " style="width: 18rem;">
				        <div class="imgOuter">
				            <img class="card-img-top" data-animation="flipInY" data-timeout="400" src="assets/img/category/wavy-hair-look.jpg" alt="messy hair look">
				        </div>
				        <div class="card-body">
				            <input type="hidden" name="service_at" value='.$row['service_at'].'>
				            <input type="hidden" name="service_id" value="'.$row['service_id'].'">
				            <input type="hidden" name="service_provider_id" value='.$row['service_provider_id'].'>
				            <h5 class="card-title">'.$row['name'].'</h5>
				            <h6 class="card-title">'.$row['service_provider_name'].'</h6>
				            <p class="card-text">'.$row['description'].'</p>
				            <a href="#" data-toggle="modal" data-target="#bookModal" class="_btn book">Book</a>
				        </div>
				    </div>';
		}