<?php 

	include("../config/dbConnection.php");
	include("functions.php");

	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
	//Retrieving Form Fields
	$user_id = $_POST['user_id'];
	$old_password = $_POST['old_password'];
	$new_password = $_POST['new_password'];

	if($con)
    {
    	$sql = "SELECT password FROM user WHERE user_id = '$user_id'";
    	$rowsAffected = mysqli_query($con,$sql);

    	while($row = mysqli_fetch_array($rowsAffected))
		{
			$password = $row['password'];
		}

		if($old_password == $password)
		{
			$sql1 = "UPDATE user SET password = '$new_password'
					WHERE user_id = '$user_id' ";
	    
	        $rowsAffected1 = mysqli_query($con,$sql1);

	        if($rowsAffected1 > 0)
	        {
	        	$data = array();
				$data["status"] = 1;
				$data["message"] = "Password changed successfully.";
	        } 
	        else
	        {
	        	$data = array();
				$data["status"] = 2;
				$data["message"] = "Something went wrong.";
	        }
		}
		else
		{
			$data = array();
			$data["status"] = 2;
			$data["message"] = "Incorrect old password.";
		}
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	} 

	$result = array("status"=>"200","data"=>$data);
	echo json_encode($result);
	
?>