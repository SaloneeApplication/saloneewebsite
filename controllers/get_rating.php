<?php 

	include("../config/dbConnection.php");

	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	
	$service_provider_id = $_POST['service_provider_id'];
	$service_id = $_POST['service_id'];
	$user_id = $_POST['user_id'];

    $sql1 = "SELECT * FROM service_provider 
    		WHERE service_provider_id = '$service_provider_id'";
    $recordSet = mysqli_query($con,$sql1);

    $data = array();
    while($row1 = mysqli_fetch_array($recordSet))
	{
		$service_provider = array();
		$service_provider["service_provider_id"] = $row1["service_provider_id"];
		$service_provider["business_name"] = $row1["business_name"];
		array_push($data, $service_provider);
	}

	$sql2 = "SELECT * FROM ratings
    		WHERE user_id = '$user_id' AND service_id = '$service_id'";
    $recordSet2 = mysqli_query($con,$sql2);
	
	$data1 = array();
    while($row2 = mysqli_fetch_array($recordSet2))
	{
		$ratings = array();
		$ratings["rating"] = $row2["rating"];
		$ratings["comment"] = $row2["comment"];
		array_push($data, $ratings);
	}
	
	$result = array("status"=>"200","service_provider"=>$data);
	echo json_encode($result);
?>