<?php 

	include("../config/dbConnection.php");
	include("functions.php");

	$dbObject = new dbConnection();
	
	$con = $dbObject->getConnection();
	$funcObject = new functions();
	
	//Retrieving Form Fields
	$user_id = $_POST['user_id'];
	$user_otp = $_POST['otp1'].$_POST['otp2'].$_POST['otp3'].$_POST['otp4'];

	if($con)
    {
    	$sql = "SELECT otp from user WHERE user_id = '$user_id'";
    	$recordSet = mysqli_query($con,$sql);

    	while($row = mysqli_fetch_array($recordSet))
		{
		    $otp = $row["otp"];
		}

		if($otp == $user_otp)
		{
			$sql = "UPDATE user SET is_mobile_verified = 1 WHERE user_id = '$user_id'";
			$rowsAffected = mysqli_query($con,$sql);

	        if($rowsAffected > 0)
	        {
	        	$recordSet = $funcObject->getUserDetails($con, $user_id);
				while($row = mysqli_fetch_array($recordSet))
				{
				    $username = $row["name"];
				    $email = $row["email"];
				    $mobile = $row["mobile"];
				    $dob = $row["dob"];
				    $image = $row["image"];
				    $area = $row["area"];
				}

				$_SESSION['user_id'] = $user_id;
				$_SESSION['username'] = $username;
				$_SESSION['email'] = $email;
				$_SESSION['mobile'] = $mobile;
				$_SESSION['image'] = $image;
				$_SESSION['area'] = $area;
				
	        	echo TRUE;
	        } 
	        else
	        {
	        	echo 'Something went wrong';
	        }
		}
		else
		{
			echo 'Incorrect OTP';
		}  
	}
	else
	{
		echo mysqli_errno()."<br/>".mysqli_error();
	} 
	
?>