<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
<style type="text/css">

    body
    {
        background:#f2f2f2;
    }

    .payment
    {
        border:1px solid #f2f2f2;
        height:300px;
        border-radius:20px;
        background:#fff;
    }
   .payment_header
   {
       background:#d06690;
       padding:20px;
       border-radius:20px 20px 0px 0px;
       
   }
   
   .check
   {
       margin:0px auto;
       width:50px;
       height:50px;
       border-radius:100%;
       background:#fff;
       text-align:center;
   }
   
   .check i
   {
       vertical-align:middle;
       line-height:50px;
       font-size:30px;
   }

    .content 
    {
        text-align:center;
    }

    .content  h1
    {
        font-size:25px;
        padding-top:25px;
    }

    .content a
    {
        width:200px;
        height:35px;
        color:#fff;
        border-radius:30px;
        padding:5px 10px;
        background:#d06690;
        transition:all ease-in-out 0.3s;
    }

    .content a:hover
    {
        text-decoration:none;
        background:#000;
    }
   
</style>
<div class="container">
   <div class="row">
      <div class="col-md-6 mx-auto mt-5">
         <div class="payment">
            <div class="payment_header">
               <div class="check"><i class="fa fa-<?php if(@$_GET['status'] == 'Success') { echo 'check';} else { echo 'close'; }?>" aria-hidden="true"></i></div>
            </div>
            <div class="content">
               <?php 
               $status = @$_GET['status'];
               if($status == 'Success')
               {    
                    echo '<h1>Payment Success !</h1>';
                    echo '<p>Thank you. Your booking has been confirmed.</p>';
                    echo '<p>This page will auto redirect in <span id="counter"><b>5</b></span> seconds. If you are not redirected within 5 seconds, click below button.</p>';

                    header("Refresh:5; url=booking-history.php");

                    echo '<a href="booking-history.php">Go to Salonee Website</a>';
               }
               else if($status == 'Aborted')
               {
                    echo '<h1>Payment Aborted !</h1>';
                    echo '<p>Thank you for shopping with us. We will keep you posted regarding the status of your order through e-mail</p>';

                    echo '<a href="index.php">Go to Salonee Website</a>';
               }
               else if($status == 'Failure')
               {
                    echo '<h1>Payment Failure !</h1>';
                    echo '<p>Thank you for shopping with us.However,the transaction has been declined.</p>';
                    echo '<a href="index.php">Go to Salonee Website</a>';
               }
               else if($status == 0)
               {
                    echo '<p>Something went wrong, Please try again.</p>';
                    echo '<a href="index.php">Go to Salonee Website</a>';
               }
               else
               {
                    echo '<p>Security Error. Illegal access detected.</p>';
                    echo '<a href="index.php">Go to Salonee Website</a>';
               }
               ?>               
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
function countdown() {
    var i = parseInt(document.getElementById('counter'));
    if (parseInt(i.innerHTML)<=0) {
        location.href = 'login.php';
    }
    if (parseInt(i.innerHTML)!=0) {
        i.innerHTML = parseInt(i.innerHTML)-1;
    }
}
setInterval(function(){ countdown(); },1000);
</script>