<?php include('header.php');

$shop_details = $funcObject->shopDetails($con, $_GET['id']);

$shop_details = mysqli_fetch_assoc($shop_details);

$services = $funcObject->servicesByCategory($con, $category_id="", $_GET['id']);

?>
<div class="container-fluid">
    <div class="_header"></div>
    <nav aria-label="breadcrumb" class="_custmBrdcrmb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Shops</a></li>
            <li class="breadcrumb-item active" aria-current="page">Shop Details</li>
        </ol>
    </nav>

    <div class="nearShops">
       <div class="mL80" >
        <div class="row">
            <div class="col-md-5">
                <div class="imgOutBox">
                    <img src="<?php echo ADMIN_URL.$shop_details['image'];?>" alt=""  />
                </div> 
            </div>
             <div class="col-md-5 _slon">
                <h2 class="_font"><?php echo $shop_details['business_name'];?></h2>
                <div class="threeFive f-2x">
                    <!-- <span class="number-rating">3.5</span> -->
                    <div class="stars-outer">
                        <div class="stars-inner" style="width: 70%;"></div>
                    </div>
                </div>
                <p><i><img src="assets/img/icons/clock.png" alt="clock" /></i><span> Opens at 10:00 AM</span>
                <p><i><img src="assets/img/icons/loc.png" alt="loc" /></i><span> <?php echo $shop_details['address'];?></span></p>
                           
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="ourSrvice">
                <!-- <h2 class="_font">Our Services</h2> -->
                    <!-- <ul>
                        <?php
                        foreach($services as $row)
                        {?>
                            <li>
                                <div class="imgBoxx">
                                    <img src="<?php echo $row['image'];?>" alt=""/>
                                    <a href="javascript"><?php echo $row['name'];?></a>
                                </div>
                            </li>
                        <?php
                        }?>
                    </ul> -->
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
 <?php include('footer.php');?>
