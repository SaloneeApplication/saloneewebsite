<?php 
include('header.php');

$categoriesWomen = $funcObject->categoriesListWomen($con, 2);
$categoriesMen = $funcObject->categoriesListMen($con, 2);
$categoriesChildren = $funcObject->categoriesListChildren($con, 2);

$shop_list = $funcObject->shopList($con);
$home_content = $funcObject->home_content($con);

$blog_details = $funcObject->blogsList($con, $limit=2);

while($data = mysqli_fetch_array($home_content))
{
	$about_us_heading = $data['about_us_heading'];
	$about_us_title = $data['about_us_title'];
	$about_us = $data['about_us'];

	$why_choose_us = $data['why_choose_us'];
	$categories_heading = $data['categories_heading'];
	$categories_title = $data['categories_title'];
	$categories_desc = $data['categories_desc'];
	$cat_women_title = $data['cat_women_title'];
	$cat_men_title = $data['cat_men_title'];
	$cat_children_title = $data['cat_children_title'];

	$compare_prices = $data['compare_prices'];
	$compare_prices_desc = $data['compare_prices_desc'];

	$choose_from_the_best = $data['choose_from_the_best'];
	$shops_desc = $data['shops_desc'];
	$apps_title = $data['apps_title'];
	$apps_desc = $data['apps_desc'];
	$payments_heading = $data['payments_heading'];
	$payments_title = $data['payments_title'];
	$payments_desc = $data['payments_desc'];
	$service_heading = $data['service_heading'];
	$service_title = $data['service_title'];
	$service_desc = $data['service_desc'];
	$welcome_text = $data['welcome_text'];
	$our_latest_news = $data['our_latest_news'];
	$news_desc = $data['news_desc'];
}

//$city_list = $funcObject->getCities($con);

?>
<rs-module-wrap id="rev_slider_1_1_wrapper" data-source="gallery">
	<rs-module id="rev_slider_1_1" style="" data-version="6.1.3" class="rev_slider_1_2_height">
		<rs-slides>
			<rs-slide data-key="rs-1" data-title="Slide" data-thumb="assets/img/slides/slider-mainbg-001.jpg" data-anim="ei:d;eo:d;s:d;r:0;t:slidingoverlayhorizontal;sl:d;">
				<img src="assets/img/slides/slider-mainbg-1.jpg" title="home-mainslider-bg001" width="1920" height="500" class="rev-slidebg" data-no-retina alt="">
				<rs-layer
					id="slider-2-slide-3-layer-2" 
					data-type="text"
					data-rsp_ch="on"
					data-xy="x:l,l,c,c;xo:50px,50px,0,0;y:m;yo:73px,73px,-20px,-4px;"
					data-text="w:normal;s:80,60,47,33;l:90,90,56,34;"
					data-frame_0="sX:0.8;sY:0.8;"
					data-frame_1="e:Power4.easeOut;st:750;sp:1000;sR:750;"
					data-frame_999="o:0;st:w;sR:7250;"
					style="z-index:11;font-family:Aleo;"
					><span class="slider-heding-title-font">It’s your <span style="color:#d06690">time</span> to <span style="color:#d06690">shine!</span></span> 
				</rs-layer>
				<rs-layer
					id="slider-2-slide-3-layer-4" 
					data-type="text"
					data-rsp_ch="on"
					data-xy="x:l,l,c,c;xo:50px,50px,0,430px;y:m;yo:132px,132px,25px,32px;"
					data-text="w:normal;s:30,30,16,9;l:28,28,17,10;"
					data-vbility="t,t,t,f"
					data-frame_0="sX:0.8;sY:0.8;"
					data-frame_1="e:Power4.easeOut;st:1020;sp:1000;sR:1020;"
					data-frame_999="o:0;st:w;sR:6980;"
					style="z-index:12;font-family:Aleo;"
					>All your salon services under one roof
				</rs-layer>
			</rs-slide>
			<rs-slide data-key="rs-3" data-title="Slide" data-thumb="assets/img/slides/slider-mainbg-001.jpg" data-anim="ei:d;eo:d;s:d;r:0;t:slidingoverlayhorizontal;sl:d;">
				<img src="assets/img/slides/slider-mainbg-001.jpg" title="home-mainslider-bg002" width="1920" height="500" class="rev-slidebg" data-no-retina alt="">
				<rs-layer
					id="slider-2-slide-4-layer-2" 
					data-type="text"
					data-rsp_ch="on"
					data-xy="x:l,l,c,c;xo:50px,50px,0,0;y:m;yo:73px,73px,-20px,-4px;"
					data-text="w:normal;s:80,60,47,33;l:90,90,56,34;"
					data-frame_0="sX:0.8;sY:0.8;"
					data-frame_1="e:Power4.easeOut;st:750;sp:1000;sR:750;"
					data-frame_999="o:0;st:w;sR:7250;"
					style="z-index:11;font-family:Aleo;"
					><span class="slider-heding-title-font">To <span style="color:#d06690">look</span> their very <span style="color:#d06690">best</span></span> 
				</rs-layer>
				<rs-layer
					id="slider-2-slide-4-layer-4" 
					data-type="text"
					data-rsp_ch="on"
					data-xy="x:l,l,c,c;xo:50px,50px,0,430px;y:m;yo:132px,132px,25px,32px;"
					data-text="w:normal;s:30,30,16,9;l:28,28,17,10;"
					data-vbility="t,t,t,f"
					data-frame_0="sX:0.8;sY:0.8;"
					data-frame_1="e:Power4.easeOut;st:1020;sp:1000;sR:1020;"
					data-frame_999="o:0;st:w;sR:6980;"
					style="z-index:12;font-family:Aleo;"
					>All your salon services under one roof at your doorstep. 
				</rs-layer>
			</rs-slide>
		</rs-slides>
	</rs-module>
</rs-module-wrap>
<section class="ttm-row zero-padding-section clearfix res-1199-mt-0 position-relative z-1 mt_100">
	<div class="container">
		<div class="row no-gutters box-shadow1 ttm-bgcolor-transp">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12 m-auto text-center">
						<form class="">
							<div class="input-group p-2">
								<input type="text" class="form-control search-input" autocomplete="off" id="search-input" placeholder="Search">
								<div class="input-group-append align-items-center">
									<a href="#" class="input-group-text fa fa-search" style="font-size: 35px; line-height: 45px;"></a>
								</div>
							</div>
							<div id="suggesstion-area" class="text-left"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="site-main pt-50">
	<section id="aboutus-box" class="ttm-row aboutus-section clearfix mt-100">
		<div class="container">
			<div class="row">
				<div class="col-md-6 res-767-pt-40">
					<div class="spacing-1 ttm-bgcolor-grey">
						<div class="section-title with-desc clearfix">
							<div class="title-header">
								<h2 class="title2 text-center revealOnScroll" data-animation="fadeInRight" data-timeout="400"><?php echo @$about_us_title; ?></h2>
								<hr class="three-dot-hr" />
								<h4 class="text-center m-3 revealOnScroll" data-animation="fadeInRight" data-timeout="400"> <?php echo @$about_us_heading; ?></h4>
							</div>
							<div class="title-desc revealOnScroll" data-animation="fadeInLeft" data-timeout="400">
								<?php echo @$about_us; ?>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 res-767-center">                            
					<img src="assets/img/bg-image/col-bgimage-1.png" class="img-fluid revealOnScroll" data-animation="zoomInUp" data-timeout="400" alt="bg-image">
				</div>
			</div>
		</div>
	</section>
	<section id="category-box" class="ttm-row aboutus-section clearfix">
		<div class="container-fluid">
			<div class="mrl-md-5rem">
				<div class="row">
					<div class="col-lg-7 col-md-7 col-sm-9 m-auto">
						<div class="section-title with-desc text-center clearfix">
							<div class="title-header">
								<h5 class="ttm-textcolor-skincolor text-center revealOnScroll" data-animation="fadeInRight" data-timeout="400"><?php echo @$why_choose_us; ?></h5>
								<h2 class="title revealOnScroll" data-animation="fadeInRight" data-timeout="400"><?php echo @$categories_heading; ?></h2>
								<hr class="three-dot-hr" />
							</div>
							<div class="title-desc revealOnScroll" data-animation="fadeInLeft" data-timeout="400">
								<h6 class="text-center"><strong>Book Appointment Online</strong></h6>
								<?php echo @$categories_desc; ?>
							</div>
						</div>
					</div>
				</div>

		<ul class="myList">
					<li>
					<div class="row">
							<div class="col-md-12">
								<h4 class="cat_hdng" ><?php echo @$cat_for_women; ?></h4>
							</div>
							<?php
							while($row = mysqli_fetch_array($categoriesWomen))
							{?>
								<div class="col-md-6">
									<div class="ttm-author-widget text-center ttm-bgcolor-grey m-3">
										<div class="featured-imagebox style2 rounded-circle revealOnScroll" data-animation="bounceIn" data-timeout="400">
											<div class="img_block">
												<img  src="<?php echo ADMIN_URL.$row['image'];?>" alt="image">
												<div class="ttm-icon-box">
													<button class="btn-sm ttm-btn ttm-btn-style-fill ttm-btn-color-skincolor ml-auto"><a href="services.php?category_id=<?php echo $row['category_id'];?>" style="background: none !important;">BOOK</a></button>
												</div>
											</div>
										</div>
										<div class="author-name">
											<h4 class="mb-0"><?php echo $row['name'];?></h4>
										</div>
									</div>
								</div>
							<?php
							}?>
						</div>
					</li>
					<li>
					<div class="row">
							<div class="col-md-12">
								<h4 class="cat_hdng"><?php echo @$cat_for_women; ?></h4>
							</div>
							<?php
							while($row = mysqli_fetch_array($categoriesMen))
							{?>
								<div class="col-md-6">
									<div class="ttm-author-widget text-center ttm-bgcolor-grey m-3">
										<div class="featured-imagebox style2 rounded-circle revealOnScroll" data-animation="bounceIn" data-timeout="400">
											<div class="img_block">
												<img class="" src="<?php echo ADMIN_URL.$row['image'];?>" alt="image">
												<div class="ttm-icon-box">
													<button class="btn-sm ttm-btn ttm-btn-style-fill ttm-btn-color-skincolor ml-auto"><a href="services.php?category_id=<?php echo $row['category_id'];?>" style="background: none !important;">BOOK</a></button>
												</div>
											</div>
										</div>
										<div class="author-name">
											<h4 class="mb-0"><?php echo $row['name'];?></h4>
										</div>
									</div>
								</div>
							<?php
							}?>
						</div>
					</li>
					<li>
					<div class="row">
							<div class="col-md-12">
								<h4 class="cat_hdng"><?php echo @$cat_for_women; ?></h4>
							</div>
							<?php
							while($row = mysqli_fetch_array($categoriesChildren))
							{?>
								<div class="col-md-6">
									<div class="ttm-author-widget text-center ttm-bgcolor-grey m-3">
										<div class="featured-imagebox style2 rounded-circle revealOnScroll" data-animation="bounceIn" data-timeout="400">
											<div class="img_block">
												<img class="" src="<?php echo ADMIN_URL.$row['image'];?>" alt="image">
												<div class="ttm-icon-box">
													<button class="btn-sm ttm-btn ttm-btn-style-fill ttm-btn-color-skincolor ml-auto"><a href="services.php?category_id=<?php echo $row['category_id'];?>" style="background: none !important;">BOOK</a></button>
												</div>
											</div>
										</div>
										<div class="author-name">
											<h4 class="mb-0"><?php echo $row['name'];?></h4>
										</div>
									</div>
								</div>
							<?php
							}?>
						</div>
					</li>
				</ul>
				
				<div class="text-center">
					<a href="categories.php" class="btnViewMore">View More</a>
				</div>
			</div>
		</div>
	</section>
	<!-- <section id="pricecompare-box" class="p-4 styleservices-section ttm-bgcolor-skincolor res-991-pb-100 clearfix">
        <div class="container">
            <div class="row section-title with-desc clearfix mt-50">
            	<div class="col-md-5 pt-4">
                    <div class="title-header overflow-hidden p-4"> 
                        <h2 class="title ttm-textcolor-white revealOnScroll" data-animation="fadeInLeft" data-timeout="400"><?php echo @$compare_prices; ?></h2>
                        <p class="revealOnScroll" data-animation="fadeInRight" data-timeout="400"><?php echo @$compare_prices_desc; ?></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="title-desc ttm-textcolor-white border-left pl-30 pr-30 pt-10 pb-10 mt-30 res-767-p-0 res-767-mt-0 res-767-b-0">
                    	<select class="form-control ttm-textcolor-white revealOnScroll" data-animation="fadeInRight" data-timeout="400" style="width: 100%;">
						  <option value="volvo">Male Hair Setting</option>
						  <option value="saab">Option-1</option>
						  <option value="fiat">Option-2</option>
						  <option value="audi">Option-3</option>
						</select>
					</div>
                </div>
            </div>
    	</div>
    </section>
    <section class="ttm-row testimonial-section bg-img2 p-0 clearfix">
        <div class="container-fluid">
			<div class="mrl-md-5rem">
				<div class="row">
					<div class="col-12 p-4">
						<div class="services-slide  mt_100 res-1199-mt_70 revealOnScroll" data-animation="fadeInDown" data-timeout="400" data-item="6" data-nav="true" data-dots="false" data-auto="false">
							<div class="service-box">
								 <div class="featured-imagebox featured-imagebox-post style3">
									<div class="ttm-post-thumbnail featured-thumbnail"> 
										<img class="img-fluid" src="assets/img/pro/logo-1.jpg" alt="image"> 
									</div>
									<div class="featured-content featured-content-post">
										<div> 
											<span class="ttm-meta-line"><i class="fa fa-map-marker"></i> Kavuri Hills, Madhapur</span> 
										</div>
										<div class="post-title featured-title">
											<h5><a href="#">PRICE: 50 AED</a></h5>
										</div>
										<div> 
											<span class="ttm-meta-line"><i class="fa fa-paperclip"></i> <a href="#"><strong>RATE CARD FOR ALL</strong></a></span>
										</div>
										<div class="post-footer mb-3 text-center">
											<button class="btn-sm ttm-btn ttm-btn-style-fill ttm-btn-color-skincolor ml-auto">BOOK</button>                                 
										</div>
									</div>
								</div>
							</div>
							<div class="service-box">
								 <div class="featured-imagebox featured-imagebox-post style3">
									<div class="ttm-post-thumbnail featured-thumbnail"> 
										<img class="img-fluid" src="assets/img/pro/logo-1.jpg" alt="image"> 
									</div>
									<div class="featured-content featured-content-post">
										<div> 
											<span class="ttm-meta-line"><i class="fa fa-map-marker"></i> Kavuri Hills, Madhapur</span> 
										</div>
										<div class="post-title featured-title">
											<h5><a href="#">PRICE: 50 AED</a></h5>
										</div>
										<div> 
											<span class="ttm-meta-line"><i class="fa fa-paperclip"></i> <a href="#"><strong>RATE CARD FOR ALL</strong></a></span>
										</div>
										<div class="post-footer mb-3 text-center">
											<button class="btn-sm ttm-btn ttm-btn-style-fill ttm-btn-color-skincolor ml-auto">BOOK</button>                                 
										</div>
									</div>
								</div>
							</div>
							<div class="service-box">
								 <div class="featured-imagebox featured-imagebox-post style3">
									<div class="ttm-post-thumbnail featured-thumbnail"> 
										<img class="img-fluid" src="assets/img/pro/logo-1.jpg" alt="image"> 
									</div>
									<div class="featured-content featured-content-post">
										<div> 
											<span class="ttm-meta-line"><i class="fa fa-map-marker"></i> Kavuri Hills, Madhapur</span> 
										</div>
										<div class="post-title featured-title">
											<h5><a href="#">PRICE: 50 AED</a></h5>
										</div>
										<div> 
											<span class="ttm-meta-line"><i class="fa fa-paperclip"></i> <a href="#"><strong>RATE CARD FOR ALL</strong></a></span>
										</div>
										<div class="post-footer mb-3 text-center">
											<button class="btn-sm ttm-btn ttm-btn-style-fill ttm-btn-color-skincolor ml-auto">BOOK</button>                                 
										</div>
									</div>
								</div>
							</div>
							<div class="service-box">
								 <div class="featured-imagebox featured-imagebox-post style3">
									<div class="ttm-post-thumbnail featured-thumbnail"> 
										<img class="img-fluid" src="assets/img/pro/logo-1.jpg" alt="image"> 
									</div>
									<div class="featured-content featured-content-post">
										<div> 
											<span class="ttm-meta-line"><i class="fa fa-map-marker"></i> Kavuri Hills, Madhapur</span> 
										</div>
										<div class="post-title featured-title">
											<h5><a href="#">PRICE: 50 AED</a></h5>
										</div>
										<div> 
											<span class="ttm-meta-line"><i class="fa fa-paperclip"></i> <a href="#"><strong>RATE CARD FOR ALL</strong></a></span>
										</div>
										<div class="post-footer mb-3 text-center">
											<button class="btn-sm ttm-btn ttm-btn-style-fill ttm-btn-color-skincolor ml-auto">BOOK</button>                                 
										</div>
									</div>
								</div>
							</div>
							<div class="service-box">
								 <div class="featured-imagebox featured-imagebox-post style3">
									<div class="ttm-post-thumbnail featured-thumbnail"> 
										<img class="img-fluid" src="assets/img/pro/logo-1.jpg" alt="image"> 
									</div>
									<div class="featured-content featured-content-post">
										<div> 
											<span class="ttm-meta-line"><i class="fa fa-map-marker"></i> Kavuri Hills, Madhapur</span> 
										</div>
										<div class="post-title featured-title">
											<h5><a href="#">PRICE: 50 AED</a></h5>
										</div>
										<div> 
											<span class="ttm-meta-line"><i class="fa fa-paperclip"></i> <a href="#"><strong>RATE CARD FOR ALL</strong></a></span>
										</div>
										<div class="post-footer mb-3 text-center">
											<button class="btn-sm ttm-btn ttm-btn-style-fill ttm-btn-color-skincolor ml-auto">BOOK</button>                                 
										</div>
									</div>
								</div>
							</div>
							<div class="service-box">
								 <div class="featured-imagebox featured-imagebox-post style3">
									<div class="ttm-post-thumbnail featured-thumbnail"> 
										<img class="img-fluid" src="assets/img/pro/logo-1.jpg" alt="image"> 
									</div>
									<div class="featured-content featured-content-post">
										<div> 
											<span class="ttm-meta-line"><i class="fa fa-map-marker"></i> Kavuri Hills, Madhapur</span> 
										</div>
										<div class="post-title featured-title">
											<h5><a href="#">PRICE: 50 AED</a></h5>
										</div>
										<div> 
											<span class="ttm-meta-line"><i class="fa fa-paperclip"></i> <a href="#"><strong>RATE CARD FOR ALL</strong></a></span>
										</div>
										<div class="post-footer mb-3 text-center">
											<button class="btn-sm ttm-btn ttm-btn-style-fill ttm-btn-color-skincolor ml-auto">BOOK</button>                                 
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
    </section> -->
                        
	<section id="feature-box" class="ttm-row beauty-service-section bg-img4 pb-10">
		<div class="beauty-service-title-wrapper">
			<div class="container">
				<div class="row">
					<div class="col-lg-7 col-md-7 col-sm-9 m-auto">
						<div class="section-title with-desc text-center clearfix">
							<div class="title-header">
								<h2 class="title revealOnScroll" data-animation="fadeInLeft" data-timeout="400" ><?php echo @$choose_from_the_best; ?></h2>
								<hr class="three-dot-hr" />
							</div>
							<h6 class=" revealOnScroll" data-animation="fadeInRight" data-timeout="400" >
								<?php echo @$shops_desc; ?>
							</h6>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="ttm-row bg-img1 contact-section res-767-p-15 clearfix mb-0">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12">
						<div class="mrl-md-5rem">
							<?php
							if($shop_list->num_rows > 0)
							{?>
								<div class="services2-slide owl-carousel mt_100 revealOnScroll" data-animation="fadeInDown" data-timeout="400" data-item="5" data-nav="true" data-dots="false" data-auto="false">
									<?php
									foreach($shop_list as $row)
									{?>
										<div class="featured-imagebox featured-imagebox-post style3">
											<div class="ttm-post-thumbnail featured-thumbnail salon-thumb"> 
												<img class="img-fluid" src="<?php echo ADMIN_URL.$row['image'];?>" alt="image"> 
											</div>
											<div class="featured-content featured-content-post salon-card-base">
												<div class="salon-summary">
													<div class="post-title featured-title">
														<h5><a href="#"><?php echo $row['business_name'];?></a></h5>
													</div>
													<div>
														<span class="ttm-meta-line"><strong>4.5</strong> <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i></span> 
													</div>
													<div> 
														<span class="ttm-meta-line"><i class="fa fa-map-marker"></i> <?php echo substr($row['address'], 0,25);?></span> 
													</div>
													<div> 
														<span class="ttm-meta-line"><i class="fa fa-clock-o"></i> Opens at 10:00 AM</span>
													</div>
												</div>
												<div>
													<a href="shop-details.php?id=<?php echo $row['service_provider_id'];?>" class="_btn book view-salon">View</a>
												</div>
											</div>
										</div>
									<?php
									}?>
								</div>
								<div class="text-center mb-50">
									<a href="saloon-shops.php" class="btnViewMore">View More</a>
								</div>
							<?php
							}
							else
							{?>
								<div class="text-center">
									<h5 style="margin-top: -75px;" class="ttm-textcolor-skincolor">No Services found in your location.</h5>
								</div>
							<?php
							}?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="download-box" class="ttm-row fid-section ttm-bgcolor-darkgrey ttm-bg ttm-bgimage-yes bg-img11 clearfix">
		<div class="ttm-row-wrapper-bg-layer ttm-bg-layer"></div>
		<div class="container">
			<div class="row ttm-fid-row-wrapper p-5">
				<div class="col-md-12">
					<div class="overlay-white-dense padding-75px text-center md-padding-25px revealOnScroll" data-animation="fadeInDown" data-timeout="400">
						<h2 class="textwidget widget-text ttm-textcolor-white "><?php echo @$apps_title; ?></h2>
						<!-- <p class="pb-10 res-767-p-0"><?php echo @$apps_desc; ?></p> -->
					</div>
				</div>
				<div class="col-8 offset-2">
					<div class="ttm-tabs tabs-style-03">
						<ul class="tabs clearfix">
							<li class="tab revealOnScroll" data-animation="fadeInRight" data-timeout="400" ><a href="#" class="app_link"><img class="img-fluid" src="assets/img/download-app-store.png" alt=""></a></li>
							<li class="tab revealOnScroll" data-animation="fadeInLeft" data-timeout="400" ><a href="#" class="app_link"><img class="img-fluid" src="assets/img/download-google-play.png" alt=""></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="ttm-row aboutus-section clearfix">
		<div class="container">
			<div class="row">
				<div class="col-md-6 res-767-center">
					<img src="assets/img/bg-image/col-bgimage-12.jpg" class="img-fluid revealOnScroll" data-animation="flipInY" data-timeout="400" alt="bg-image">
				</div>
				<div class="col-md-6 res-767-pt-40">
					<div class="spacing-8">
						<div class="section-title with-desc clearfix">
							<div class="title-header">
								<h5 class="ttm-textcolor-skincolor text-center revealOnScroll" data-animation="fadeInRight" data-timeout="400"><?php echo @$payments_heading; ?></h5>
								<h2 class="title2 text-center revealOnScroll" data-animation="fadeInLeft" data-timeout="200"><?php echo @$payments_title; ?></h2>
								<hr class="three-dot-hr" />
							</div>
							<div class="title-desc revealOnScroll" data-animation="fadeInRight" data-timeout="400"><?php echo @$payments_desc;?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="ttm-row aboutus-section clearfix">
		<div class="container">
			<div class="row">
				<div class="col-md-6 res-767-pt-40">
					<div class="ttm-bgcolor-grey p-3">
						<div class="section-title with-desc clearfix">
							<div class="title-header">
								<h5 class="ttm-textcolor-skincolor text-center revealOnScroll" data-animation="fadeInLeft" data-timeout="400"><?php echo @$service_heading;?></h5>
								<h2 class="title2 text-center revealOnScroll" data-animation="fadeInRight" data-timeout="400"><?php echo @$service_title;?></h2>
								<hr class="three-dot-hr" />
							</div>
							<div class="title-desc revealOnScroll" data-animation="fadeInLeft" data-timeout="400"><?php echo @$service_desc;?></div>
						</div>
					</div>
				</div>
				<div class="col-md-6 res-767-center">                            
					<img src="assets/img/bg-image/col-bgimage-4.jpg" class="img-fluid revealOnScroll" data-animation="flipInY" data-timeout="400" alt="bg-image">
				</div>
			</div>
		</div>
	</section>
	<section class="news-section clearfix" id="blog">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-7 col-sm-9 m-auto text-center pt-90 res-991-pt-40 res-767-pt-0">
					<div class="section-title with-desc clearfix">
						<div class="title-header">
							<!-- <h5 class="ttm-textcolor-skincolor revealOnScroll" data-animation="fadeInLeft" data-timeout="400">Welcome</h5> -->
							<h2 class="title revealOnScroll" data-animation="fadeInRight" data-timeout="400">BLOG SECTION</h2>
							<hr class="three-dot-hr" />
						</div>
					</div>
				</div>
				<div class="row">
					<?php
					foreach($blog_details as $row)
					{?>
						<div class="col-lg-6 col-md-6">
							<div class="row featured-imagebox featured-imagebox-post style3 revealOnScroll" data-animation="fadeInDown" data-timeout="500">
								<div class="col-md-7 ttm-post-thumbnail featured-thumbnail p-0">
									<img class="img-fluid" src="<?php echo ADMIN_URL.$row['image'];?>" alt="image">
								</div>
								<div class="col-md-5 featured-content featured-content-post">
                                    <div class="post-meta"> <span class="ttm-meta-line"><i class="fa fa-calendar"></i><?php echo date('M d, Y', strtotime($row['created_date']));?></span>
                                    </div>
                                    <div class="post-title featured-title">
                                        <h5><a href="blog-details.php?id=<?php echo $row['blog_id'];?>"><?php echo $row['name'];?></a></h5>
                                    </div>
                                    <div class="featured-desc">
                                        <p><?php echo substr($row['short_desc'], 0, 300).'...';?></p>
                                    </div>
                                    <div class="post-footer"> <span class="ttm-meta-line"><a class="ttm-textcolor-skincolor" href="blog-details.php?id=<?php echo $row['blog_id'];?>">read more&nbsp;&nbsp;<i class="fa fa-angle-right"></i></a></span>
                                    </div>
                                </div>
							</div>
						</div>
					<?php
					}?>
				</div>
			</div>
	</section>
	<!-- <section class="news-section clearfix">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-7 col-sm-9 m-auto text-center pt-90 res-991-pt-40 res-767-pt-0">
					<div class="section-title with-desc clearfix">
						<div class="title-header">
							<h5 class="ttm-textcolor-skincolor revealOnScroll" data-animation="fadeInLeft" data-timeout="400"><?php echo @$welcome_text;?></h5>
							<h2 class="title revealOnScroll" data-animation="fadeInRight" data-timeout="400"><?php echo @$our_latest_news;?></h2>
							<hr class="three-dot-hr" />
						</div>
						<div class="title-desc revealOnScroll" data-animation="fadeInLeft" data-timeout="400"><?php echo @$news_desc;?></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-6">
					<div class="row featured-imagebox featured-imagebox-post style3 revealOnScroll" data-animation="fadeInDown" data-timeout="200">
						<div class="col-md-7 ttm-post-thumbnail featured-thumbnail p-0"> 
							<img class="img-fluid" src="assets/img/pro/05.jpg" alt="image"> 
						</div>
						<div class="col-md-5 featured-content featured-content-post">
							<div class="post-meta">
								<span class="ttm-meta-line"><i class="fa fa-calendar"></i>February 10, 2020</span>
							</div>
							<div class="post-title featured-title">
								<h5><a href="#">How to Extend The Life of Your Haircolor</a></h5>
							</div>
							<div class="featured-desc">
								<p>Quaerat neque purus ipsum neque dolor primis libero tempus impedit tempor at blandit sapien gravida donee ipsum undo porta justo</p>
							</div>
							<div class="post-footer">
								<span class="ttm-meta-line"><a class="ttm-textcolor-skincolor" href="blog-1.php">read more&nbsp;&nbsp;<i class="fa fa-angle-right"></i></a></span>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6">
					<div class="row featured-imagebox featured-imagebox-post style3 revealOnScroll" data-animation="fadeInDown" data-timeout="500">
						<div class="col-md-7 ttm-post-thumbnail featured-thumbnail p-0"> 
							<img class="img-fluid" src="assets/img/pro/06.jpg" alt="image"> 
						</div>
						<div class="col-md-5 featured-content featured-content-post">
							<div class="post-meta">
								<span class="ttm-meta-line"><i class="fa fa-calendar"></i>February 10, 2020</span>
							</div>
							<div class="post-title featured-title">
								<h5><a href="#">Quaerat neque purus ipsum neque dolor primis libero</a></h5>
							</div>
							<div class="featured-desc">
								<p>Quaerat neque purus ipsum neque dolor primis libero tempus impedit tempor at blandit sapien gravida donee ipsum undo porta justo</p>
							</div>
							<div class="post-footer">
								<span class="ttm-meta-line"><a class="ttm-textcolor-skincolor" href="blog-1.php">read more&nbsp;&nbsp;<i class="fa fa-angle-right"></i></a></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> -->
</div>
<?php include('footer.php'); ?>
<style>
#OTPform input:focus-within {
color: black!important;
background-color: #d66292;
}
</style>