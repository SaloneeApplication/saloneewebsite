<?php 
include('header.php'); 
$page = 'discounts';

$user_id = @$_SESSION['user_id'];

if($user_id == "")
{
    echo '<script> var base_url = "http://localhost/salonee_web/"; </script>';
    echo '<script> window.location.replace(base_url); </script>';
}

include("config/dbConnection.php");
include("config/constants.php");
include("controllers/functions.php");

$dbObject = new dbConnection();
$con = $dbObject->getConnection();
$funcObject = new functions();

$discounts = $funcObject->getDiscounts($con);
?>

<div class="container-fluid">
  <div class="_header"></div>
  <!-- breadcrumb  -->
  <nav aria-label="breadcrumb" class="_custmBrdcrmb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="#">Home</a></li>
      <li class="breadcrumb-item"><a href="#">My Account</a></li>
      <li class="breadcrumb-item active" aria-current="page">Discounts</li>
    </ol>
  </nav>

<div class="d-flex myFlex">
  <?php include('sidebar.php');?>

    <div class="mainDiv _bgWyt">
        <?php
        foreach($discounts as $row)
        {?>
            <div class="card _bookngsCrd _discount">
               <div class="crdImg">
                 <img src="./assets/img/discount.jpg" alt="discount">
               </div>
               <div class="crd_desc"> 
                  <h5 class="card-title"><?php echo $row['discount_per'];?>% Off </h5>
                  <p>Coupon Code : <b><?php echo $row['coupon_code'];?></b></p>
                  <p><?php echo $row['short_desc'];?></p>  
               </div>        
            </div>
        <?php
        }?>
    </div> <!--end Main Div-->


</div>
<?php include('footer.php');?>